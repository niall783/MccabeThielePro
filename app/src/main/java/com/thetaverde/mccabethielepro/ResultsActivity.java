package com.thetaverde.mccabethielepro;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;



import org.w3c.dom.Text;


public class ResultsActivity extends AppCompatActivity  {






    static Intent stagesGraphIntent;
    static  Intent rminGraphIntent;

    //initializing the textViews for the answers
    TextView feedFlowRateAnsTV;
    TextView lightFeedFlowRateAnsTV;
    TextView heavyFeedFlowRateAnsTV;

    TextView topProductFlowRateAnsTV;
    TextView bottomProdFlowRateAnsTV;
    TextView liquidflowTopColumnAnsTV;
    TextView vapourflowTopColumnAnsTV;
    TextView liquidflowBottomColumnAnsTV;
    TextView vapourflowBottomColumnAnsTV;

    TextView numOfPlatesAnsTV;
    TextView minRefluxRatioAnsTV;

    TextView stagesGraphOpLinePtOfIntersectAnsTV;
    TextView rMinGraphOpLinePtOfIntersectAnsTV;

    TextView feedRateUnitsTextView;
    TextView lightFeedFlowRateUnitsTextView;
    TextView heavyFeedFlowRateUnitsTextView;

    TextView topProductFlowRateUnitsTextView;
    TextView botProductFlowRateUnitsTextView;
    TextView topProductHeavyFlowRateUnitsTextView;
    TextView topProductLightFlowRateUnitsTextView;
    TextView botProductHeavyFlowRateUnitsTextView;
    TextView botProductLightFlowRateUnitsTextView;


    Intent intent;



    String unitsOfFlowRates;


    String minRefluxSwitchState;
    static String RefluxRatio;
    static String q;
    static String xd;
    static String xb;
    static String xf;


    //needed only for the worked solution
    static String units_flowRate;
    static String noOfStages;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.results_page_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.show_rmin_graph:
                showRminGraph(rMinGraphOpLinePtOfIntersectAnsTV);//random view used here
                return true;

            case R.id.worked_solution:
                showWorkedSolution(topProductFlowRateUnitsTextView); //random view user here
                return true;

            case R.id.show_stages_graph:
                showStagesGraph(rMinGraphOpLinePtOfIntersectAnsTV); //random view used here
                return true;
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;

            default:
                return false;
        }




    }








    public void showStagesGraph(View view){
         stagesGraphIntent = new Intent(getApplicationContext(),GraphViewActivity.class);

        //passing the data from the Main Activity to the Graph Activity in order to plot the graphs
        stagesGraphIntent.putExtra("refluxRatio",intent.getStringExtra("refluxRatio"));
        stagesGraphIntent.putExtra("q",intent.getStringExtra("q"));
        stagesGraphIntent.putExtra("xd",intent.getStringExtra("xd")); // top product light component fraction
        stagesGraphIntent.putExtra("xb",intent.getStringExtra("xb"));// bottom product light component fraction
        stagesGraphIntent.putExtra("xf",intent.getStringExtra("xf"));// fraction of light component in the feed
        stagesGraphIntent.putExtra("minRefluxState",intent.getStringExtra("minRefluxState"));
        stagesGraphIntent.putExtra("typeOfGraph","Stages"); //Variable to decide which graph to show in the graph activity

        SearchActivity.sharedPreferences.edit().putString("typeOfGraph","Stages").apply(); //so that the right labels are put on the correct graph


        startActivity(stagesGraphIntent);
    }



    public void showRminGraph(View view){
        rminGraphIntent = new Intent(ResultsActivity.this,GraphViewActivity.class);

        //passing the data from the Main Activity to the Graph Activity in order to plot the graphs
        rminGraphIntent.putExtra("refluxRatio",intent.getStringExtra("refluxRatio"));
        rminGraphIntent.putExtra("q",intent.getStringExtra("q"));
        rminGraphIntent.putExtra("xd",intent.getStringExtra("xd")); // top product light component fraction
        rminGraphIntent.putExtra("xb",intent.getStringExtra("xb"));// bottom product light component fraction
        rminGraphIntent.putExtra("xf",intent.getStringExtra("xf"));// fraction of light component in the feed
        rminGraphIntent.putExtra("minRefluxState",intent.getStringExtra("minRefluxState"));
        rminGraphIntent.putExtra("typeOfGraph","Rmin"); //Variable to decide which graph to show in the graph activity

        SearchActivity.sharedPreferences.edit().putString("typeOfGraph","Rmin").apply(); //so that the right labels are put on the correct graph

        startActivity(rminGraphIntent);

    }

    public void showWorkedSolution(View view){




        Intent workedSolutionIntent = new Intent(ResultsActivity.this,WorkedSolutionActivity.class);
        startActivity(workedSolutionIntent);


    }






    @Override
    public void onResume() {




        super.onResume();



    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        setTitle("Results");







        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

        //initializing the textViews for the answers
        feedFlowRateAnsTV = (TextView) findViewById(R.id.feedFlowRateAnsTV);
        lightFeedFlowRateAnsTV = (TextView) findViewById(R.id.lightFeedFlowRateAnsTV);
        heavyFeedFlowRateAnsTV = (TextView) findViewById(R.id.heavyFeedFlowRateAnsTV);

        topProductFlowRateAnsTV = (TextView) findViewById(R.id.topProductFlowRateAnsTV);
        bottomProdFlowRateAnsTV = (TextView) findViewById(R.id.bottomProdFlowRateAnsTV);
        vapourflowTopColumnAnsTV = (TextView) findViewById(R.id.vapourflowTopColoumnAnsTV);
        liquidflowTopColumnAnsTV = (TextView) findViewById(R.id.liguidflowTopColoumnAnsTV);
        vapourflowBottomColumnAnsTV = (TextView) findViewById(R.id.vapourflowBottomColoumnAnsTV);
        liquidflowBottomColumnAnsTV = (TextView) findViewById(R.id.liquidflowBottomColoumnAnsTV);

        numOfPlatesAnsTV = (TextView) findViewById(R.id.numOfPlatesAnsTV);
        minRefluxRatioAnsTV = (TextView) findViewById(R.id.minRefluxRatioAnsTV);

        stagesGraphOpLinePtOfIntersectAnsTV = (TextView) findViewById(R.id.stagesGraphOpLinePtOfIntersectAnsTV);
        rMinGraphOpLinePtOfIntersectAnsTV = (TextView) findViewById(R.id.rMinGraphOpLinePtOfIntersectAnsTV);

        //initializing the units textViews - just for displaying the units
        feedRateUnitsTextView = (TextView) findViewById(R.id.feedRateUnitsTextView);
        lightFeedFlowRateUnitsTextView = (TextView) findViewById(R.id.lightFeedFlowRateUnitsTextView);
        heavyFeedFlowRateUnitsTextView = (TextView) findViewById(R.id.heavyFeedFlowRateUnitsTextView);
        topProductFlowRateUnitsTextView = (TextView) findViewById(R.id.topProductFlowRateUnitsTextView);
        botProductFlowRateUnitsTextView = (TextView) findViewById(R.id.botProductFlowRateUnitsTextView);
        topProductHeavyFlowRateUnitsTextView = (TextView)  findViewById(R.id.topProductHeavyFlowRateUnitsTextView);
        topProductLightFlowRateUnitsTextView = (TextView) findViewById(R.id.topProductLightFlowRateUnitsTextView);
        botProductHeavyFlowRateUnitsTextView = (TextView) findViewById(R.id.botProductHeavyFlowRateUnitsTextView);
        botProductLightFlowRateUnitsTextView = (TextView) findViewById(R.id.botProductLightFlowRateUnitsTextView);


        intent = getIntent();

        //was getting a null error from this

        minRefluxSwitchState = intent.getStringExtra("minRefluxState");
        RefluxRatio = intent.getStringExtra("refluxRatio");
        q = intent.getStringExtra("q");
        xd = intent.getStringExtra("xd");
        xb = intent.getStringExtra("xb");
        xf = intent.getStringExtra("xf");









        //Writing to the textViews
        // Writing to the textViews
        //Writing to the textViews

        //Flow Rate Answers
        feedFlowRateAnsTV.setText(intent.getStringExtra("FeedRate")); //feed flow rate
        lightFeedFlowRateAnsTV.setText(intent.getStringExtra("lightCompFeedRate")); //light component feed flow rate
        heavyFeedFlowRateAnsTV.setText(intent.getStringExtra("heavyCompFeedRate")); // heavy component feed flow rate

        topProductFlowRateAnsTV.setText(intent.getStringExtra("vapourFlowRate"));  //Distillate flow rate
        bottomProdFlowRateAnsTV.setText(intent.getStringExtra("liquidFlowRate"));  //Bottoms flow rate

        vapourflowTopColumnAnsTV.setText(intent.getStringExtra("lightCompDist")); //flow rate of light component in the Distillate
        liquidflowTopColumnAnsTV.setText(intent.getStringExtra("heavyCompDist")); //flow rate of heavy component in the Distillate

        vapourflowBottomColumnAnsTV.setText(intent.getStringExtra("lightCompBot")); //flow rate of light component in the Bottoms
        liquidflowBottomColumnAnsTV.setText(intent.getStringExtra("heavyCompBot")); //flow rate of heavy component in the Bottoms



        //units variable - notice the space below - the " "
        unitsOfFlowRates = " " + intent.getStringExtra("feedFlowRateUnits"); //units of the flow rate

        //writing to the units textViews
        feedRateUnitsTextView.setText(unitsOfFlowRates);
        lightFeedFlowRateUnitsTextView.setText(unitsOfFlowRates);
        heavyFeedFlowRateUnitsTextView.setText(unitsOfFlowRates);
        topProductFlowRateUnitsTextView.setText(unitsOfFlowRates);
        botProductFlowRateUnitsTextView.setText(unitsOfFlowRates);
        topProductHeavyFlowRateUnitsTextView.setText(unitsOfFlowRates);
        topProductLightFlowRateUnitsTextView.setText(unitsOfFlowRates);
        botProductHeavyFlowRateUnitsTextView.setText(unitsOfFlowRates);
        botProductLightFlowRateUnitsTextView.setText(unitsOfFlowRates);




        //number of plates
        //number of plates

        //initializing the below object - mb the correct name
        GraphViewActivity graphActivityCalc = new GraphViewActivity();

        //initializing the below object - mb in order to use the "round" method in Main Activity
        MainActivity formatNumbers = new MainActivity();

        String numberOfPlates = String.valueOf(graphActivityCalc.getNumberOfPlates(RefluxRatio,q,xd,xb,xf,minRefluxSwitchState) + " ");

        Double MinRefluxRatio = graphActivityCalc.getMinRefluxRatio(RefluxRatio,q,xd,xb,xf,minRefluxSwitchState);

        //writing the value fo the minimum reflux to the testView
        minRefluxRatioAnsTV.setText(formatNumbers.round(MinRefluxRatio,4));

        numOfPlatesAnsTV.setText(String.valueOf(numberOfPlates));



        //needed only for the worked solution
        units_flowRate = intent.getStringExtra("feedFlowRateUnits");
        noOfStages = numberOfPlates;



                //operating lines point of intersection for the stages graph
        Double intersectPtForStagesGraph[][] = graphActivityCalc.getStagesGraphOperatingLineIntersectionPoint();




        //the code below look for the decimal separator. Either a "," or "."
        //if it is  "," then the coordinate separator should be a ";"
        //if it is a "." then the coordinate separator should be a ","
        String coordinateSeparator = ",";

        //reading in an example x coordinate to see if the decimal separator is a "," of "."
        String xCoord = formatNumbers.round(intersectPtForStagesGraph[0][0],3);

        for (int i = 0 ; i < xCoord.length();i++){

            char c = xCoord.charAt(i);

            if (c == ','){
                coordinateSeparator = ";";
            }

            }



        stagesGraphOpLinePtOfIntersectAnsTV.setText("(" + formatNumbers.round(intersectPtForStagesGraph[0][0],2)+ coordinateSeparator + formatNumbers.round(intersectPtForStagesGraph[0][1],2) + ")" );


        //operating lines point of intersection for the stages graph
        Double intersectPtForRminGraph[][] = graphActivityCalc.getRminGraphOperatingLineIntersectionPoint();

        rMinGraphOpLinePtOfIntersectAnsTV.setText("(" + formatNumbers.round(intersectPtForRminGraph[0][0],2)+ coordinateSeparator + formatNumbers.round(intersectPtForRminGraph[0][1],2) + ")" );




    }






}
