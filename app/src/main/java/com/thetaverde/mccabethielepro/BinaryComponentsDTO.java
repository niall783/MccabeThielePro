package com.thetaverde.mccabethielepro;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by niall_000 on 01/05/2017.
 */

//DTO = Data transfer Object
public class BinaryComponentsDTO {


    private String userSelectedBinaryMixtureName;
    private String sqliteBinaryMixtureName;



    private  Double[] arrayXvals;
    private  Double[] arrayYvals;


    public String getUserSelectedBinaryMixtureName() {
        return userSelectedBinaryMixtureName;
    }

    public void setUserSelectedBinaryMixtureName(String userSelectedBinaryMixtureName) {
        this.userSelectedBinaryMixtureName = userSelectedBinaryMixtureName;
    }













    public Double[] getArrayXvals() {
        return arrayXvals;
    }

    public void setArrayXvals(Double[] arrayXvals) {
        this.arrayXvals = arrayXvals;
    }

    public Double[] getArrayYvals() {
        return arrayYvals;
    }

    public void setArrayYvals(Double[] arrayYvals) {
        this.arrayYvals = arrayYvals;
    }





    public String getSqliteBinaryMixtureName() {
        return sqliteBinaryMixtureName;
    }

    public void setSqliteBinaryMixtureName(String sqliteBinaryMixtureName) {
        this.sqliteBinaryMixtureName = sqliteBinaryMixtureName;
        Log.i("DTO",sqliteBinaryMixtureName);
    }
}
