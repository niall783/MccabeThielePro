package com.thetaverde.mccabethielepro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static java.lang.Math.abs;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class GraphViewActivity extends AppCompatActivity  {





    int lineThickness = 8;

//the three dots menu at the top of the activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.graph_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    //the three dots menu at the top of the activity

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         super.onOptionsItemSelected(item);

        Intent graphSettingsIntent = new Intent(this,GraphSettingsActivity.class);

        switch (item.getItemId()){

            case R.id.set_graph_title:
                startActivity(graphSettingsIntent);
                finish(); // this is so that when the user clicks the back button it returns to the results page
                return  true;

            case R.id.set_axis_labels:
                startActivity(graphSettingsIntent);
                finish(); // this is so that when the user clicks the back button it returns to the results page
                return true;

            default: return false;

        }



    }


    Bitmap bitmap;


    //needed only for the worked solution
    static String yIntercept_static;

    //initializing the reflux ratio, q line, xd and xb value which will be read in from the Results Activity
    Double refluxRatio;
    Double q;
    Double xd; // fraction of light component in the distillate
    Double xb; // fraction of ligh component in the bottoms
    Double xf; //fraction of light component in the feed
    Boolean minRefluxSwitchState; //minimum reflux Switch State;

    //initializing the arraydata arrays that will be populated from the SQLite database
    Double[] arrayXvals;
    Double[] arrayYvals;

    String typeOfGraph; ////Variable to decide which graph to show in the graph activity

    int paddingLeft = 95; // the padding for left side of the graph that allows the y-axis value to be seen  //was 95

    // the below coordinate is calculated in the plot bottom operating line method
    Double pointOfIntersection_x; // x coordinate of the point of intersection of the q line and the top operating line or the equlibrium line when calculating Rmin
    Double pointOfIntersection_y; // y coordinate of the point of intersection of the q line and the top operating line or the equlibrium line when calculating Rmin

    Intent intent;

    GraphView graph;

    Double randomQLineYVal; // random q line  end val that i picked // this possibly need defined better

    //defining the stage data points here so that i can access them everywhere
    LineGraphSeries<DataPoint> stageOfColumn_Horizonal;
    LineGraphSeries<DataPoint> stageOfColumn_Vertical;

    //defining the top Operating line datapoints  here so that i can access them everywhere
    LineGraphSeries<DataPoint> topOperatingLineRminDataPoints;




    //Generating the 45 degree Line Data
    public LineGraphSeries fortyFiveDegreeLineData(){

        LineGraphSeries<DataPoint> fortyFiveDegreeLine = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0,0),
                new DataPoint(1,1)
        });

        fortyFiveDegreeLine.setColor(Color.RED);

        //fortyFiveDegreeLine.setThickness(lineThickness);

        return fortyFiveDegreeLine;
    }



    public LineGraphSeries QLineData() {

        randomQLineYVal = 0.9; // random q line  end val that i picked // this possibly need defined better // defined in onCreate
        int numberOfPointsInQline = 2; // the number of Points in the q line // need in order th define the size of the array

        Double xValueForEndOfQLine = (((randomQLineYVal*(1-q))/(-q))+(xf/q));

        DataPoint[] qLineDataPoints = new DataPoint[numberOfPointsInQline];

        //if the x values int the datapoint array don't go up in order then the graph will not plot
        // he below if - else ensure that the the two x vals fgo up in acceding order

        //when q is = 0 the graph doesn't plot correctly so i put this if statement in
        if(q != 0) {

            if (xf < xValueForEndOfQLine) {
                qLineDataPoints[0] = new DataPoint(xf, xf);
                qLineDataPoints[1] = new DataPoint(xValueForEndOfQLine, randomQLineYVal);
            } else {
                qLineDataPoints[0] = new DataPoint(xValueForEndOfQLine, randomQLineYVal);
                qLineDataPoints[1] = new DataPoint(xf, xf);

            }

        }else{
            qLineDataPoints[0] = new DataPoint(0, xf);
            qLineDataPoints[1] = new DataPoint(xf, xf);
        }

        LineGraphSeries<DataPoint> qLineGraphPoints = new LineGraphSeries<>(qLineDataPoints);

        qLineGraphPoints.setColor(Color.CYAN);

       // qLineGraphPoints.setThickness(lineThickness);

        return qLineGraphPoints;
    }





    //plotting the top operating line
    public LineGraphSeries TopOperatingLineData(Double R){


        ArrayList<Double> xVals = new ArrayList<>();
        ArrayList<Double> yVals = new ArrayList<>();

        // the x values that are needed
        xVals.add(0.0);
        xVals.add(xd);

        // adding the y values that are needed to the yVals array list
        for(int i = 0 ; i < xVals.size(); i++) {
            yVals.add((R / (R + 1)) * (xVals.get(i)) + (1 / (R + 1)) * xd);
        }

        LineGraphSeries<DataPoint> topOperatingLinePoints = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(xVals.get(0),yVals.get(0)),
                new DataPoint(xVals.get(1),yVals.get(1))

        });

        topOperatingLinePoints.setColor(Color.BLUE);

        //topOperatingLinePoints.setThickness(lineThickness);

        return topOperatingLinePoints;

    }




    public LineGraphSeries<DataPoint> BottomOperatingLineData(Boolean plottingRminGraph){

        // if we are not plotting the Rmin graph then redefine the point of intersection as follows
        if (!plottingRminGraph) {
            //when i put the q line equation and the top operating line equation equal to each other
            // I got the following equation which gives me the x coordinate of the point of intersection
            //between the q line and the top operating line

            pointOfIntersection_x = (xf * (refluxRatio + 1) - xd * (1 - q)) / (refluxRatio * (1 - q) + q * (refluxRatio + 1));

            //putting the x coordinate of the point of intersection back into the top operating line equation to find the y coordinate
            pointOfIntersection_y = (refluxRatio / (refluxRatio + 1)) * (pointOfIntersection_x) + (1 / (refluxRatio + 1)) * xd;
        }

        LineGraphSeries<DataPoint> bottomOperatingLinePoints = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(xb, xb), //this data point will always be the same
                new DataPoint(pointOfIntersection_x, pointOfIntersection_y)

        });



        bottomOperatingLinePoints.setColor(Color.GREEN);

       // bottomOperatingLinePoints.setThickness(lineThickness);


        return bottomOperatingLinePoints;

    }




    public LineGraphSeries EqulibriumCurveData(){

        //getting the equilibrium array values
        arrayXvals = SearchActivity.arrayXvals;
        arrayYvals = SearchActivity.arrayYvals;



        //populating the equlibriumDataPoints array with the equlibrium data points
        int count = arrayXvals.length;

        DataPoint[] equlibriumDataPoints = new DataPoint[count];

        for(int i = 0 ; i < count;i++){

            DataPoint v = new DataPoint(arrayXvals[i],arrayYvals[i]);

            equlibriumDataPoints[i] = v;


        }

        LineGraphSeries<DataPoint> equlibriumCurvePoints = new LineGraphSeries<>(equlibriumDataPoints);

        equlibriumCurvePoints.setColor(Color.MAGENTA);

       // equlibriumCurvePoints.setThickness(lineThickness);


        return equlibriumCurvePoints;

    }




    public int getStageData() {


        Double gradientOfBottomOperatingLine = (pointOfIntersection_y - xb) / (pointOfIntersection_x - xb);



        //getting the equilibrium array values
        arrayXvals = SearchActivity.arrayXvals;
        arrayYvals = SearchActivity.arrayYvals;





        //array list of data Points for the coordinates of the stages
        ArrayList<DataPoint> coordsOfStages = new ArrayList<>();


        //Adding the first point here as it is known
        //If any xd adjustments are need then this value is overwritten
        coordsOfStages.add(new DataPoint(xd,xd));

        //initializing the the  initial previousYVal with the y coordinate of the first stage
        Double previousYVal = xd;
        Double nextXVal = 1.0;
        Double nextYVal = 0.0;
        int numberOfStages = 0;

        Boolean topOfGraphNotCheck = true; //check to see if the equlibrium curve and the top operating line are too close together for drawing stages

        Boolean adjustedXdValNotFound = true;

        Boolean tolPointAdjustmentNotMade = true;

        while (xb < nextXVal) {


            Boolean pointNotChosen = true;



            Boolean pointNotChosenForAdjustedXdValue = true; //if the user enters an xd value that is too high the app with calculate thousands of stages and crash
            //therefore the xd value has to be changed to something more appropriate - see below




            for (int i = 1; i < arrayXvals.length; i++) {


                if ((arrayYvals[i] > previousYVal)  && pointNotChosen ) {

                    nextXVal = (arrayXvals[i] + arrayXvals[i - 1]) / 2;



                    Double limit = 0.01;
                    Double currentYVal = previousYVal;



                    if ((abs(nextXVal - currentYVal) < limit) && topOfGraphNotCheck){



                        do{


                            currentYVal = currentYVal - 0.02;


                            for (int j = 1; j < arrayXvals.length; j++) {


                                if ((arrayYvals[j] > currentYVal) && pointNotChosenForAdjustedXdValue && adjustedXdValNotFound) {
                                    nextXVal = (arrayXvals[j] + arrayXvals[j - 1]) / 2;
                                    pointNotChosenForAdjustedXdValue = false;


                                }

                                if (abs(currentYVal - nextXVal) > limit) {
                                    topOfGraphNotCheck = false;
                                    previousYVal = currentYVal;
                                    adjustedXdValNotFound = false;

                                    coordsOfStages.clear();
                                    //adding the first point for the stages here when the xd value has to be adjusted because it too close to the top of the graph which cause the app too crash because of too many stages being calculated
                                    coordsOfStages.add(new DataPoint(previousYVal,previousYVal));



                                    break;
                                }

                            }

                            pointNotChosenForAdjustedXdValue = true;

                        }while(topOfGraphNotCheck);


                    }
                    numberOfStages = numberOfStages +1;





                    coordsOfStages.add(new DataPoint(nextXVal, previousYVal));




                    //this is the min allowable difference between the Top operating line and the equlibrium curve, this is to stop the app crashing by calculating too many stages.
                    Double minAllowedDifferenceBetweenEqulibAndTolCurves = 0.01;

                    //amount that the nextXVal is reduced by to try and find a mewt the criteria below
                    Double reduceBy = 0.005;



                    // if - else statement is so figure out when the stage pass the intersection of the bottom and top operating lines with the q line
                    if (pointOfIntersection_x < nextXVal) {
                        //third point in this stage and first point in the next next stage

                        nextYVal = ((refluxRatio / (refluxRatio + 1)) * (nextXVal) + (1 / (refluxRatio + 1)) * xd);



                        //if the vertical distance between the Top Operating line and the equlibrium curve is too small
                        //then the app crashes because too many stages are calculated - hence the if statement
                        if(previousYVal - nextYVal > minAllowedDifferenceBetweenEqulibAndTolCurves){

                            coordsOfStages.add(new DataPoint(nextXVal, nextYVal));

                        }else{
                            //this while loop recalculates a more appropriate first stage location

                            do {
                                nextXVal = nextXVal - reduceBy;

                                nextYVal = ((refluxRatio / (refluxRatio + 1)) * (nextXVal) + (1 / (refluxRatio + 1)) * xd);


                                for (int h = 1 ; h < arrayXvals.length ; h++) {

                                    if ((arrayXvals[h] > nextXVal)) {
                                        previousYVal = (arrayYvals[h] + arrayYvals[h - 1]) / 2;

                                        break;

                                    }
                                }

                                if(previousYVal - nextYVal > minAllowedDifferenceBetweenEqulibAndTolCurves){

                                    tolPointAdjustmentNotMade = false;


                                    coordsOfStages.clear();

                                    coordsOfStages.add(new DataPoint(previousYVal, previousYVal));
                                    coordsOfStages.add(new DataPoint(nextXVal, previousYVal));
                                    coordsOfStages.add(new DataPoint(nextXVal, nextYVal));

                                }



                            }while (tolPointAdjustmentNotMade);
                        }



                    } else if (pointOfIntersection_x > nextXVal) {


                        // straight forward y = mx + c
                        nextYVal = gradientOfBottomOperatingLine * (nextXVal) - xb * gradientOfBottomOperatingLine + xb;

                        //override so that the final line for the last stage end on the 45 degree line
                        if (xb > nextXVal){
                            nextYVal = nextXVal;
                        }
                        coordsOfStages.add(new DataPoint(nextXVal, nextYVal));

                    }


                    previousYVal = nextYVal;


                    pointNotChosen = false;

                }

            }
        }


        // converting the array list of datapoints to an array of data points
        // also, reordering the array to be the opposite of the array list
        // the x values need to be in ascending order
        DataPoint[] coordOfStagesArray_Vertical = new DataPoint[coordsOfStages.size()];
        DataPoint[] coordOfStagesArray_Horizontal = new DataPoint[coordsOfStages.size()];


        int currentSizeOfDataPointArrayList = coordsOfStages.size() - 1;

        //for some reason i need two arrays in order to plot the vertical and horizontal line
        //this is the vertical one
        for (int i = 0; i < coordsOfStages.size(); i++) {
            coordOfStagesArray_Vertical[i] = coordsOfStages.get(currentSizeOfDataPointArrayList - i);
        }

        //for some reason i need two arrays in order to plot the vertical and horizontal line
        //this is the horizontal one
        for (int i = 0; i < coordsOfStages.size(); i++) {
            coordOfStagesArray_Horizontal[i] = coordsOfStages.get(i);
        }

        //i need to LineGraph series to plot the vertical and horizontaol lines for the stages
        stageOfColumn_Horizonal = new LineGraphSeries<>(coordOfStagesArray_Horizontal);
        stageOfColumn_Vertical = new LineGraphSeries<>(coordOfStagesArray_Vertical);

        stageOfColumn_Vertical.setColor(Color.BLACK);
        stageOfColumn_Horizonal.setColor(Color.BLACK);

       // stageOfColumn_Horizonal.setThickness(lineThickness);
       // stageOfColumn_Vertical.setThickness(lineThickness);


        return numberOfStages;

    }





    public Double getMinReflux(){


        //getting the equilibrium array values
        arrayXvals = SearchActivity.arrayXvals;
        arrayYvals = SearchActivity.arrayYvals;





        randomQLineYVal = 0.9; // random q line y end val that i picked // this possibly need defined better // defined in onCreate
        int numberOfDataPoints = 1000; // number of data points for the q line

        //qline equation
        Double xValueForEndOfQLine = (((randomQLineYVal * (1 - q)) / (-q)) + (xf / q)); // equation is x in terms of y i.e. x = y

        //the first point on the qline will always be (xf,xf)
        Double qlinePointInterval = (randomQLineYVal - xf) / numberOfDataPoints; //calculating the interval between the points fot the Qline

        ArrayList<Double> qlineXVals = new ArrayList<>(); //array List to store generated qline x vals
        ArrayList<Double> qlineYVals = new ArrayList<>(); // array list to store values from xd to 1

        ArrayList<Double> arrayXVals_short = new ArrayList<>(); // shortened array list of equlibrium value where the q line could cross
        ArrayList<Double> arrayYVals_short = new ArrayList<>(); // shortened array list of equlibrium value where the q line could cross


        qlineXVals.clear();
        qlineYVals.clear();

        arrayXVals_short.clear();
        arrayYVals_short.clear();


        //first value i the qline in always the same
        qlineXVals.add(xf);
        qlineYVals.add(xf);


        // for loop populates the qlineXVals and qlineYVals with the appropriate numbers
        for (int i = 0; i < numberOfDataPoints; i++) {

            //if statements prevents the below equation from dividing by zero
            if(q != 0){
                qlineYVals.add(qlineYVals.get(i) + qlinePointInterval);


                qlineXVals.add((((qlineYVals.get(i + 1) * (1 - q)) / (-q)) + (xf / q)));



            }else{

                qlinePointInterval = (xf-0)/numberOfDataPoints;

                qlineYVals.add(xf);

                qlineXVals.add(qlineXVals.get(i)-qlinePointInterval);

            }

        }

        //if statements prevents the below equation from dividing by zero
        if(q != 0){
            //the last value of the qlineYVals array is slightly inaccurate, out by about 0.0000001
            //i removed the last point and added in the correct exact value as it is known
            qlineYVals.remove(qlineYVals.size() - 1);
            qlineYVals.add(randomQLineYVal);

            //the last value of the qlineXVals array is slightly inaccurate, out by about 0.0000001
            //i removed the last point and added in the correct exact value as it is known
            qlineXVals.remove(qlineXVals.size() - 1);
            qlineXVals.add(xValueForEndOfQLine);

        }else{
            //the last value of the qlineYVals array is slightly inaccurate, out by about 0.0000001
            //i removed the last point and added in the correct exact value as it is known
            qlineYVals.remove(qlineYVals.size() - 1);
            qlineYVals.add(xf);

            //the last value of the qlineXVals array is slightly inaccurate, out by about 0.0000001
            //i removed the last point and added in the correct exact value as it is known
            qlineXVals.remove(qlineXVals.size() - 1);
            qlineXVals.add(0.0);

        }





        Double buffer = 0.01; // the buffer is to ensure that all the possible intersection points are included in the array
        // for loop populates the below array lists with all the possible points of intersections
        // it omits the points that are defiantly not points of intersection

        for (int t = 0; t < arrayXvals.length; t++) {

            if (arrayYvals[t] > (xf - buffer) && arrayXvals[t] < (xf + buffer)) {



                    arrayXVals_short.add(arrayXvals[t - 1]);
                arrayYVals_short.add(arrayYvals[t - 1]);



            }

        }



        Double allowedDiff = 0.001; // allowable difference between the qline value and the arrayXVals or arrayYVals
        Boolean pointOfIntersectionNotFound = true; // Boolean to act as a latch allow only the first point that meets the allowable difference to be store
        ArrayList<Double> pointOfIntersection_x_temp = new ArrayList<>(); //Array List to store all the close x vals of the points of intersection
        ArrayList<Double> pointOfIntersection_y_temp = new ArrayList<>(); //Array List to store all the close y vals of the points of intersection

        //the following nested for loops loop through the point for the equlibrium curve and the qline
        //it compares the difference in the points until it finds some that are within the allowed difference
        //it does find more than one point that fits the criteria


        for (int p = 0; p < qlineXVals.size(); p++) {

            for (int r = 0; r < arrayXVals_short.size(); r++) {



                if ((abs(arrayXVals_short.get(r) - qlineXVals.get(p)) < allowedDiff) && (abs(arrayYVals_short.get(r) - qlineYVals.get(p)) < allowedDiff)) {


                    pointOfIntersection_x = (arrayXVals_short.get(r) + qlineXVals.get(p)) / 2;

                    pointOfIntersection_y = (arrayYVals_short.get(r) + qlineYVals.get(p)) / 2;

                    pointOfIntersection_x_temp.add(pointOfIntersection_x);
                    pointOfIntersection_y_temp.add(pointOfIntersection_y);

                }

            }
        }

        //taking the midpoint of the arraylist of possible close intersection points and then finding the middle value
        Log.i("this", String.valueOf(pointOfIntersection_x_temp.size()));

        pointOfIntersection_x = pointOfIntersection_x_temp.get(Math.round(pointOfIntersection_x_temp.size() / 2));

        pointOfIntersection_y = pointOfIntersection_y_temp.get(Math.round(pointOfIntersection_y_temp.size() / 2));






        //top operating line intersection with the y axis
        Double yIntercept = ((xd-pointOfIntersection_y)/(xd - pointOfIntersection_x))*(0-xd) + xd;

        //needed only of the worked solution activity
        yIntercept_static = String.valueOf(yIntercept);


        //yIntercept = xd/(Rmin+1)
        Double Rmin = (xd/yIntercept)-1;



        topOperatingLineRminDataPoints = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0,yIntercept),
                new DataPoint(xd,xd)

        });

        topOperatingLineRminDataPoints.setColor(Color.BLUE);

        return Rmin;

    }

    // the operating lines intersection point for the stages graph - not tested
    public Double[][] getStagesGraphOperatingLineIntersectionPoint(){


        getNumberOfPlates(refluxRatio.toString(),q.toString(),xd.toString(),xb.toString(),xf.toString(),minRefluxSwitchState.toString());

        Double intersectionPoint[][] = new Double[1][2];

        intersectionPoint[0][0] = pointOfIntersection_x;
        intersectionPoint[0][1] = pointOfIntersection_y;
        Log.i("intersect1",pointOfIntersection_x.toString());
        Log.i("intersect1",pointOfIntersection_y.toString());

        return intersectionPoint;

    }

    // the operating lines intersection point for the stages graph - not tested
    public Double[][] getRminGraphOperatingLineIntersectionPoint(){

        getMinRefluxRatio(refluxRatio.toString(),q.toString(),xd.toString(),xb.toString(),xf.toString(),minRefluxSwitchState.toString());

        Double intersectionPoint[][] = new Double[1][2];

        intersectionPoint[0][0] = pointOfIntersection_x;
        intersectionPoint[0][1] = pointOfIntersection_y;
        Log.i("intersect1",pointOfIntersection_x.toString());
        Log.i("intersect1",pointOfIntersection_y.toString());

        return intersectionPoint;

    }


    //getting the number of stage require the calling of the below method in a particular order
    public int getNumberOfPlates(String R, String q_val,String xd_val, String xb_val, String xf_val, String minRefluxBool){


        minRefluxSwitchState = Boolean.parseBoolean(minRefluxBool);
        refluxRatio = Double.parseDouble(R);
        q = Double.parseDouble(q_val);
        xd = Double.parseDouble(xd_val);
        xb = Double.parseDouble(xb_val);
        xf = Double.parseDouble(xf_val);

        QLineData();

        //if else that changes the reflux ratio that is sent to the "TopOperatingLineData" method
        //this is if the user enter the reflux ratio as a function of the Rmin i.e. Reflux Ration = coefficient * Rmin
        if (minRefluxSwitchState){
            //the number that the user has multiplied the Rmin value by. i.e. Reflux Ration = coefficient * Rmin
            Double RminCoefficient = refluxRatio;


            TopOperatingLineData(RminCoefficient*getMinReflux());
        }else{

            TopOperatingLineData(refluxRatio);
        }



        BottomOperatingLineData(false);

        fortyFiveDegreeLineData();

        EqulibriumCurveData();

        return getStageData();
    }

    public Double getMinRefluxRatio(String R, String q_val,String xd_val, String xb_val, String xf_val, String minRefluxBool){


        minRefluxSwitchState = Boolean.parseBoolean(minRefluxBool);
        refluxRatio = Double.parseDouble(R);
        q = Double.parseDouble(q_val);
        xd = Double.parseDouble(xd_val);
        xb = Double.parseDouble(xb_val);
        xf = Double.parseDouble(xf_val);

        QLineData();

        EqulibriumCurveData();

        fortyFiveDegreeLineData();



        return getMinReflux();
    }


    public void plotRminGraph(){

        graph = (GraphView) findViewById(R.id.graph);

        GridLabelRenderer glr = graph.getGridLabelRenderer();
        glr.setPadding(paddingLeft); // should allow for 3 digits to fit on screen


        //calling the method to plot the appropriate graphs
        graph.addSeries(QLineData());


        graph.addSeries(fortyFiveDegreeLineData());

        graph.addSeries(EqulibriumCurveData());

        //getMinReflux is a method that returns the min reflux within this activity it cannot be used to pass the Rmin val to a different activity
        //getMinRefluxRatio has to be used for this
        getMinReflux();

        //plotting the Rmin Top operating line
        graph.addSeries(topOperatingLineRminDataPoints);

        graph.addSeries(BottomOperatingLineData(true));


    }




    public void plotStagesGraphs(){


        graph = (GraphView) findViewById(R.id.graph);

        GridLabelRenderer glr = graph.getGridLabelRenderer();
        glr.setPadding(paddingLeft); // should allow for 3 digits to fit on screen

        //calling the method to plot the appropriate graphs
        graph.addSeries(QLineData());

        graph.addSeries(TopOperatingLineData(refluxRatio));

        graph.addSeries(BottomOperatingLineData(false));

        graph.addSeries(fortyFiveDegreeLineData());

        graph.addSeries(EqulibriumCurveData());

        getStageData();


        //the code below plots the stages.
        //the method getStagesData
        graph.addSeries(stageOfColumn_Vertical);
        graph.addSeries(stageOfColumn_Horizonal);


    }

    public void exportButton(View view){

        openExportGraphDialogueBox();

    }


    public void openExportGraphDialogueBox(/*View view*/){

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(GraphViewActivity.this);
        alertDialog.setTitle("Export Graph to PDF");
        alertDialog.setMessage("Name your File...");


        final EditText graphFileName = new EditText(GraphViewActivity.this);
        graphFileName.setHint("e.g. McCabe Thiele Graph");

        //only allowing alphabet characters and white spaces
        graphFileName.setFilters(new InputFilter[] {
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if(cs.equals("")){ // for backspace
                            return cs;
                        }
                        if(cs.toString().matches("[a-zA-Z ]+")){
                            return cs;
                        }
                        return "";
                    }
                }
        });



        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        graphFileName.setLayoutParams(lp);
        alertDialog.setView(graphFileName);
        //alertDialog.setIcon(R.drawable.key);




        alertDialog.setPositiveButton("EXPORT",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        exportGraph(graphFileName.getText().toString());

                    }
                });


        alertDialog.setNeutralButton("Title & Axis Labels", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(GraphViewActivity.this, GraphSettingsActivity.class);
                startActivity(intent);

            }
        });

        alertDialog.setNegativeButton("BACK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertDialog.show();
    }




    public void exportGraph(final String graphFileName){



        toggleHide();


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                takeScreenshot(graphFileName);
            }
        }, 1000);




    }




    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }





    private static void addImage(Document document,byte[] byteArray)
    {
        Image image = null;
        try
        {
            image = Image.getInstance(byteArray);
        }
        catch (BadElementException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MalformedURLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // image.scaleAbsolute(150f, 150f);
        try
        {
            document.add(image);
            Log.i("AddIMage","Good");
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void takeScreenshot(String graphFileName) {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);



        // image naming and path  to include sd card  appending name you choose for file
        //String mPath = Environment.getExternalStorageDirectory().toString() + "/PICTURES/Screenshots/" + now+ ".jpg";
       // String mPath = Environment.getExternalStorageDirectory().toString() + "/Documents/" + now + ".jpg";
        String mPath = Environment.getExternalStorageDirectory().toString() + "/Documents/" + "McCabeThieleSceenShot" +".jpg";


        Log.i("TakeScreenShot", " Yes");







        try {

           // v1.setDrawingCacheEnabled(false);
            final File imageFile = new File(mPath);



            //create a bitmap that is the same size as the graph view
             bitmap = Bitmap.createBitmap((int) (graph.getWidth()), (int) (graph.getHeight()), Bitmap.Config.ARGB_8888);



            Canvas canvas = new Canvas(bitmap) {
                @Override
                public boolean isHardwareAccelerated() {
                    return true;
                }
            };

            graph.draw(canvas);





            Matrix matrix = new Matrix();

            matrix.postRotate(90);

            //rotate the bitmap so that it fits on the pdf
            bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);



            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);

            outputStream.flush();
            outputStream.close();


            //setting the graphFilename if the user left this area blank
            if(graphFileName.equals("")){
                graphFileName = "McCabeThiele";
            }

            //Now create the name of your PDF file that you will generate
            //final File pdfFile = new File(Environment.getExternalStorageDirectory().toString() + "/Documents/" + now +"McCabeThiele.pdf");
            final File pdfFile = new File(Environment.getExternalStorageDirectory().toString() + "/Documents/" + graphFileName + ".pdf");


            Thread t = new Thread() {
                @Override
                public void run() {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            try {
                                Document  document = new Document(PageSize.A4);

                                PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

                                float marginTop = 10;
                                float marginBottom = 0;
                                float marginRight = 10;
                                float marginLeft = 0;

                                //setting the document/pdf margins
                                document.setMargins(marginLeft,marginRight,marginTop,marginBottom);

                                /*Log.i("PageSize", String.valueOf(document.getPageSize()));

                                Log.i("PageSizeRight", String.valueOf(document.right()));
                                Log.i("PageSizeLeft", String.valueOf(document.left()));
                                Log.i("PageSizeTop", String.valueOf(document.top()));
                                Log.i("PageSizeBottom", String.valueOf(document.bottom()));*/

                                //create a scaled bitmap so that it fits on the pdf
                                bitmap = Bitmap.createScaledBitmap(bitmap,(int) (document.right()), (int) (document.top()), true);

                                document.open();
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] byteArray = stream.toByteArray();
                                addImage(document,byteArray);
                                document.close();


                                MediaScannerConnection.scanFile(GraphViewActivity.this,new String[]{pdfFile.toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                                    @Override
                                    public void onScanCompleted(String s, Uri uri) {
                                        Log.i("ExternalStorage - pdf", "Scanned " + s + ":");
                                        Log.i("ExternalStorage - pdf", "-> uri=" + uri);

                                    }
                                });


                                    //could't get this to work
                                //deleteFile(String.valueOf(imageFile));






                                Intent intent = new Intent(Intent.ACTION_VIEW);

                                Uri uri;
                                //file provider is only needed for Build version >=24
                                if (Build.VERSION.SDK_INT >= 24) {

                                    //Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + "/Documents/",  "myPdfFile.pdf"));

                                    //Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", pdfFile);
                                    uri = FileProvider.getUriForFile(GraphViewActivity.this, getApplicationContext().getPackageName() + ".fileprovider", pdfFile);

                                    Log.i("Build","is greater than or equal to 24");
                                }else {
                                    //this bit is untested
                                    uri = Uri.fromFile(pdfFile);
                                    Log.i("Build","is less than 24");
                                }



                                //this sets the file to be opened
                                intent.setDataAndType(uri, "application/pdf");




                                Log.i("UriInfo",uri.toString());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                startActivity(intent);

                            }
                            catch (Exception e){
                                Log.i("pdfError ",e.toString());
                            }

                        }
                    });

                }
            };t.start();







            //i think this bit refreshes the images gallery
            MediaScannerConnection.scanFile(this,
                    new String[]{imageFile.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);

                        }
                    });











        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
            Log.i("Error2",e.toString());



        }











    }
























































    //this is where the full screen activity template code starts
    //this is where the full screen activity template code starts
    //this is where the full screen activity template code starts
    //this is where the full screen activity template code starts
    //this is where the full screen activity template code starts


    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };






    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    private void toggleHide() {
        if (mVisible) {
            hide();
        } else {
            hide();
        }
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }


    //this is where the fullscreen template code stops
    //this is where the fullscreen template code stops
    //this is where the fullscreen template code stops
    //this is where the fullscreen template code stops
    //this is where the fullscreen template code stops








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_graph_view);

        setTitle("McCabe Thiele Graph");





        //ensuring that the graph activity only opens in landscape mode
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //this is to verify that i have write permissions to the users device
        verifyStoragePermissions(this);




        //getting the equilibrium array values
        arrayXvals = SearchActivity.arrayXvals;
        arrayYvals = SearchActivity.arrayYvals;

        Log.i("ArrayValsx", Arrays.toString(arrayXvals));
        Log.i("ArrayValsy",Arrays.toString(arrayYvals));






        //intent that get the reflux ratio and q line value
        intent = getIntent();


        randomQLineYVal = 0.9; // random q line  end val that i picked // this possibly need defined better

        //need to add some constraints to the edit texts - mb
        //getting the reflux ratio and the q line value from the Results Activity which gets it from the Main Activity
        refluxRatio = Double.parseDouble(intent.getStringExtra("refluxRatio"));
        q = Double.parseDouble(intent.getStringExtra("q"));
        xd = Double.parseDouble(intent.getStringExtra("xd"));
        xb = Double.parseDouble(intent.getStringExtra("xb"));
        xf = Double.parseDouble(intent.getStringExtra("xf"));
        minRefluxSwitchState = Boolean.parseBoolean(intent.getStringExtra("minRefluxState"));
        typeOfGraph = intent.getStringExtra("typeOfGraph");

        graph = (GraphView) findViewById(R.id.graph);

        GridLabelRenderer glr = graph.getGridLabelRenderer();
        glr.setPadding(paddingLeft); // should allow for 3 digits to fit on screen

        //Setting the graphs axis boundaries manually
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(1);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(1);

        //enabling zooming and scrolling
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScalableY(true);
        graph.getViewport().setScrollableY(true);

        //setting the graph title - pulling the data from shared preferences
        graph.setTitle(SearchActivity.sharedPreferences.getString("graphTitle",""));
        graph.setTitleTextSize((float) 70);


        //setting xAxis label
        graph.getGridLabelRenderer().setHorizontalAxisTitle(SearchActivity.sharedPreferences.getString("xAxisLabel", ""));

        //setting yAxis label
        graph.getGridLabelRenderer().setVerticalAxisTitle(SearchActivity.sharedPreferences.getString("yAxisLabel",""));



        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.VERTICAL);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.BOTH);



        // if statement that decides which graph to plot Rmin or the Stages graph
        if (typeOfGraph.equals("Stages")){
            //method that plots the Stages graph
            plotStagesGraphs();
        }else if(typeOfGraph.equals("Rmin")){
            //method that plots the Rmin graph
            plotRminGraph();
        }


















        //below is the fullscreen activity template code
        //below is the fullscreen activity template code
        //below is the fullscreen activity template code
        //below is the fullscreen activity template code


        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.graph);


        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.export_button).setOnTouchListener(mDelayHideTouchListener);
    }










}
