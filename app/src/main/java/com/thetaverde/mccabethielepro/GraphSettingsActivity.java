package com.thetaverde.mccabethielepro;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

public class GraphSettingsActivity extends AppCompatActivity implements  TextWatcher {

    EditText graphTitle;
    EditText xAxisLabel;
    EditText yAxisLabel;


    public void setTitleAndLabels(View view){

        SearchActivity.sharedPreferences.edit().putString("graphTitle",graphTitle.getText().toString()).apply();
        SearchActivity.sharedPreferences.edit().putString("xAxisLabel",xAxisLabel.getText().toString()).apply();
        SearchActivity.sharedPreferences.edit().putString("yAxisLabel",yAxisLabel.getText().toString()).apply();



        if(SearchActivity.sharedPreferences.getString("typeOfGraph","").equals("Stages")){

            startActivity(ResultsActivity.stagesGraphIntent);
        }
        else if(SearchActivity.sharedPreferences.getString("typeOfGraph","").equals("Rmin")){

            startActivity(ResultsActivity.rminGraphIntent);
        }

        finish(); //this is so that the user cannot come back to this page by clicking the back button

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            SearchActivity.sharedPreferences.edit().putString("graphTitle",graphTitle.getText().toString()).apply();
            SearchActivity.sharedPreferences.edit().putString("xAxisLabel",xAxisLabel.getText().toString()).apply();
            SearchActivity.sharedPreferences.edit().putString("yAxisLabel",yAxisLabel.getText().toString()).apply();

            if(SearchActivity.sharedPreferences.getString("typeOfGraph","").equals("Stages")){

                startActivity(ResultsActivity.stagesGraphIntent);
            }
            else if(SearchActivity.sharedPreferences.getString("typeOfGraph","").equals("Rmin")){

                startActivity(ResultsActivity.rminGraphIntent);
            }

            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_settings);


        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }


        graphTitle = (EditText) findViewById(R.id.graphTitleEditText);
        xAxisLabel = (EditText) findViewById(R.id.xAxisLabelEditText);
        yAxisLabel = (EditText) findViewById(R.id.yAxisLabelEditText);

        graphTitle.setText(SearchActivity.sharedPreferences.getString("graphTitle",""));
        xAxisLabel.setText(SearchActivity.sharedPreferences.getString("xAxisLabel",""));
        yAxisLabel.setText(SearchActivity.sharedPreferences.getString("yAxisLabel",""));



    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        SearchActivity.sharedPreferences.edit().putString("graphTitle",graphTitle.getText().toString()).apply();
        SearchActivity.sharedPreferences.edit().putString("xAxisLabel",xAxisLabel.getText().toString()).apply();
        SearchActivity.sharedPreferences.edit().putString("yAxisLabel",yAxisLabel.getText().toString()).apply();

    }
}
