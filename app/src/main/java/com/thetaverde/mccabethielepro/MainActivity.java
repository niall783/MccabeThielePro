package com.thetaverde.mccabethielepro;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;



import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    static Intent intent;

    //needed only for the worked solution
    static String feedFlowRate_Static;


    TextView multiplyByRminTextView;

    EditText feedFlowRateEditText;
    EditText FeedFracLightEditText;
    EditText TopProdFracLightEditText;
    EditText BottomProdFracLightEditText;
    EditText RefluxRatioEditText;
    EditText qLineEditText;

    Spinner feedFlowRateSpinner;
    Spinner FeedFracLightSpinner;
    Spinner TopProdFracLightSpinner;
    Spinner BottomProdFracLightSpinner;

    Switch minRefluxSwitch;

    //minimum reflux switch state boolean
    Boolean minRefluxSwitchState;

    Button calculateButton;

    PopupWindow popupWindow;
    LayoutInflater layoutInflater;

    //method to round numbers
    public String round(Double number, int decimalPlace) {

        Log.i("InRound", "Good");

        String roundedNumber = "";

        final DecimalFormat df0 = new DecimalFormat(".");
        final DecimalFormat df1 = new DecimalFormat("#.#");
        final DecimalFormat df2 = new DecimalFormat("#.##");
        final DecimalFormat df3 = new DecimalFormat("#.###");
        final DecimalFormat df4 = new DecimalFormat("#.####");
        final DecimalFormat df5 = new DecimalFormat("#.#####");
        final DecimalFormat dfe = new DecimalFormat("0.000E0");


        // this method could use some work - perhaps using a for loop to loop through th character to find the decimal point and then do something
        if (number > 0.00001 && number < 99999) {

            if (decimalPlace == 0) {
                roundedNumber = df0.format(number);

            } else if (decimalPlace == 1) {
                roundedNumber = df1.format(number);

            } else if (decimalPlace == 2) {
                roundedNumber = df2.format(number);

            } else if (decimalPlace == 3) {
                roundedNumber = df3.format(number);


            } else if (decimalPlace == 4) {
                roundedNumber = df4.format(number);


            } else if (decimalPlace == 5) {
                roundedNumber = df5.format(number);

            }


        } else {
            roundedNumber = dfe.format(number);
        }


        return roundedNumber;

    }


    //returns the liquid(bottom) and vapour(Top) flow rates
    public String[][] flowRates(String feedFlowRate, String feedFracLight, String topProdFracLight, String bottomProdFracLight) {

        //needed only for the worked solution
        feedFlowRate_Static = feedFlowRate;

        //converting variables to doubles
        Double F = Double.parseDouble(feedFlowRate);
        Double xf = Double.parseDouble(feedFracLight);
        Double xv = Double.parseDouble(topProdFracLight);
        Double xb = Double.parseDouble(bottomProdFracLight);


        Double lightCompFeedRate = F * xf;
        Double heavyCompFeedRate = F - lightCompFeedRate;

        Double vapourFlowRate = (F * (xf - xb)) / (xv - xb); //Distillate flow rate

        Double liquidFlowRate = F - vapourFlowRate; //Bottoms flow rate

        Double lightCompFlowRate_Top = vapourFlowRate * xv;  //flow rate of light component in the Distillate
        Double heavyCompFlowRate_Top = vapourFlowRate - lightCompFlowRate_Top; //flow rate of heavy component in the Distillate

        Double lightCompFlowRate_Bot = liquidFlowRate * xb; //flow rate of light component in the Bottoms
        Double heavyCompFlowRate_Bot = liquidFlowRate - lightCompFlowRate_Bot; //flow rate of heavy component in the Bottoms


        //rounding the numbers below
        int decimalPlace = 3;

        String[][] flowRates = new String[1][10];

        flowRates[0][0] = round(vapourFlowRate, decimalPlace); //Distillate flow rate
        flowRates[0][1] = round(liquidFlowRate, decimalPlace); //Bottoms flow rate
        flowRates[0][2] = round(lightCompFlowRate_Top, decimalPlace); //flow rate of light component in the Distillate
        flowRates[0][3] = round(heavyCompFlowRate_Top, decimalPlace); //flow rate of heavy component in the Distillate
        flowRates[0][4] = round(lightCompFlowRate_Bot, decimalPlace); //flow rate of light component in the Bottoms
        flowRates[0][5] = round(heavyCompFlowRate_Bot, decimalPlace); //flow rate of heavy component in the Bottoms
        flowRates[0][6] = feedFlowRateSpinner.getSelectedItem().toString(); //units of feed flow rate
        flowRates[0][7] = round(lightCompFeedRate, decimalPlace); // light component feed rate
        flowRates[0][8] = round(heavyCompFeedRate, decimalPlace); // heavy component feed rate
        flowRates[0][9] = round(F, decimalPlace);//feed flow rate


        return flowRates;

    }



    //Calculation on Click
    public void calculateResults(View view) {
        System.out.println("in onClick");

        //getting the flowRates array here just because it's tidy
        String feedRate = feedFlowRateEditText.getText().toString();
        String feedFracLight = FeedFracLightEditText.getText().toString();
        String topProdFracLight = TopProdFracLightEditText.getText().toString();
        String bottomProdFracLight = BottomProdFracLightEditText.getText().toString();
        String refluxRatio = RefluxRatioEditText.getText().toString();
        String qLineNumber = qLineEditText.getText().toString();

        SearchActivity.sharedPreferences.edit().putString("feedRate", feedRate).apply();
        SearchActivity.sharedPreferences.edit().putString("feedFracLight", feedFracLight).apply();
        SearchActivity.sharedPreferences.edit().putString("topProdFracLight", topProdFracLight).apply();
        SearchActivity.sharedPreferences.edit().putString("bottomProdFracLight", bottomProdFracLight).apply();
        SearchActivity.sharedPreferences.edit().putString("refluxRatio", refluxRatio).apply();
        SearchActivity.sharedPreferences.edit().putString("qLineNumber", qLineNumber).apply();


        //checking to see if the minimum reflux switch has been checked
        if (minRefluxSwitch.isChecked()) {
            minRefluxSwitchState = true;
        }


        //checking to see if the user has entered actual numbers
        if (checkNumEditText(feedRate) || checkNumEditText(feedFracLight) || checkNumEditText(topProdFracLight) || checkNumEditText(bottomProdFracLight) || checkNumEditText(qLineNumber) || checkNumEditText(refluxRatio)) {
            Toast.makeText(this, "Please enter the appropriate values above.", Toast.LENGTH_SHORT).show();

            //checking to see if certain edit text are zero - because it is impossible for them to be
        } else if (zeroCheck(feedRate)) {
            Toast.makeText(this, "Feed Flow Rate cannot be zero", Toast.LENGTH_SHORT).show();
        } else if (zeroCheck(feedFracLight)) {
            Toast.makeText(this, "Feed Light Component Fraction cannot be zero", Toast.LENGTH_SHORT).show();
        } else if (zeroCheck(topProdFracLight)) {
            Toast.makeText(this, "Distillate Light Component Fraction cannot be zero", Toast.LENGTH_SHORT).show();
        } else if (zeroCheck(bottomProdFracLight)) {
            Toast.makeText(this, "Bottom Light Component Fraction cannot be zero", Toast.LENGTH_SHORT).show();
        } else if (zeroCheck(refluxRatio)) {
            Toast.makeText(this, "The Reflux Ratio cannot be zero", Toast.LENGTH_SHORT).show();

            //checking to see if the fractions entered are between 0 and 1
        } else if (rangeZeroToOneCheck(feedFracLight)) {
            Toast.makeText(this, "Feed Light Component Fraction must be between 0 and 1", Toast.LENGTH_SHORT).show();
        } else if (rangeZeroToOneCheck(topProdFracLight)) {
            Toast.makeText(this, "Distillate Light Component Fraction must be between 0 and 1", Toast.LENGTH_SHORT).show();
        } else if (rangeZeroToOneCheck(bottomProdFracLight)) {
            Toast.makeText(this, "Bottom Light Component Fraction must be between 0 and 1", Toast.LENGTH_SHORT).show();
        } else if (rangeZeroToOneCheck(qLineNumber)) {
            Toast.makeText(this, "The q value must be between 0 and 1", Toast.LENGTH_SHORT).show();

        } else if (feedFracLight.equals(topProdFracLight) && topProdFracLight.equals(bottomProdFracLight)) {
            Toast.makeText(this, "The feed fraction cannot equal the Top Product and Bottom Product fraction", Toast.LENGTH_SHORT).show();
        } else if (Double.parseDouble(topProdFracLight) < Double.parseDouble(feedFracLight)) {
            Toast.makeText(this, "The Top Product fraction cannot be less that the Feed fraction", Toast.LENGTH_SHORT).show();
        } else if (Double.parseDouble(topProdFracLight) < Double.parseDouble(bottomProdFracLight)) {
            Toast.makeText(this, "The Top Product fraction cannot be less that the Bottom product fraction", Toast.LENGTH_SHORT).show();
        } else if (Double.parseDouble(bottomProdFracLight) > Double.parseDouble(feedFracLight)) {
            Toast.makeText(this, "The Bottom fraction cannot be greater that the Feed Fraction", Toast.LENGTH_SHORT).show();
        } else if (Double.parseDouble(bottomProdFracLight) == Double.parseDouble(feedFracLight) ) {
            Toast.makeText(this, "The Bottom fraction cannot equal the the Feed Fraction", Toast.LENGTH_SHORT).show();
        } else if (Double.parseDouble(topProdFracLight) == Double.parseDouble(feedFracLight) ) {
            Toast.makeText(this, "The Top fraction cannot equal the the Feed Fraction", Toast.LENGTH_SHORT).show();
        } else {

            String flowRates[][] = flowRates(feedRate, feedFracLight, topProdFracLight, bottomProdFracLight);

             intent = new Intent(getApplicationContext(), ResultsActivity.class);

            //passing the flowRates Array to the results activity here

            intent.putExtra("vapourFlowRate", flowRates[0][0]); //Distillate flow rate
            intent.putExtra("liquidFlowRate", flowRates[0][1]); //Bottoms flow rate
            intent.putExtra("lightCompDist", flowRates[0][2]); //flow rate of light component in the Distillate
            intent.putExtra("heavyCompDist", flowRates[0][3]); //flow rate of heavy component in the Distillate
            intent.putExtra("lightCompBot", flowRates[0][4]); //flow rate of light component in the Bottoms
            intent.putExtra("heavyCompBot", flowRates[0][5]); //flow rate of heavy component in the Bottoms
            intent.putExtra("feedFlowRateUnits", flowRates[0][6]); // feed flow rate units
            intent.putExtra("lightCompFeedRate", flowRates[0][7]); // light component feed rate
            intent.putExtra("heavyCompFeedRate", flowRates[0][8]); // heavy component feed rate
            intent.putExtra("FeedRate", flowRates[0][9]); //  feed flow rate
            intent.putExtra("xd", topProdFracLight); // top product light component fraction
            intent.putExtra("xb", bottomProdFracLight); // bottom product light component fraction
            intent.putExtra("refluxRatio", refluxRatio); //refluc Ratio
            intent.putExtra("q", qLineNumber); //q line number
            intent.putExtra("xf", feedFracLight); // fraction of light component in the feed
            intent.putExtra("minRefluxState", minRefluxSwitchState.toString()); // minimum reflux switch boolean


            startActivity(intent);

        }
    }


    //formatting methods

    //method to check if a number edit text is actually a number
    public Boolean checkNumEditText(String editTextString) {

        if (editTextString.length() == 0 || editTextString.equals(".") || editTextString.equals(",") || editTextString.equals("-") || editTextString.equals("0.")) {
            return true;
        } else {
            return false;
        }

    }

    //method for checking if the value entered is zero
    public Boolean zeroCheck(String editTextString) {

        if (editTextString.equals("0")) {
            return true;
        }
        return false;
    }

    //method checking that the range of the number entered is between zero and one
    public Boolean rangeZeroToOneCheck(String editTextString) {

        Double editTextDouble = Double.parseDouble(editTextString);

        if (editTextDouble > 1 || editTextDouble < 0) {
            return true;
        } else {

            return false;
        }


    }

    //onClick for Rmin "What's this" pop up window
    public void whatsThis(View view){



        ScrollView mainActivityScrollView = (ScrollView) findViewById(R.id.mainActivityScrollView);

        LinearLayout testLayout = (LinearLayout) findViewById(R.id.testLayout);

        layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        ViewGroup container = (ViewGroup) layoutInflater.inflate(R.layout.rmin_pop_up_window,null);


        DisplayMetrics displayMetrics = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = (int) (displayMetrics.widthPixels * 0.7); //set pop up to 80% of the device width
        int height = (int) (displayMetrics.heightPixels * 0.7);  //set pop up to 80% of the device height

        //location of pop up window on the screen
        int xLocation = (displayMetrics.widthPixels - width)/2;
        int yLocation = (displayMetrics.heightPixels - height)/2;


        popupWindow = new PopupWindow(container,width,height,true);
        popupWindow.showAtLocation(mainActivityScrollView, Gravity.NO_GRAVITY,xLocation,yLocation);




        container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                popupWindow.dismiss();
                return false;
            }
        });

    }

    public void test(View view){

        Intent intent = new Intent(this, WorkedSolutionActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Enter Fractions");




        if(getSupportActionBar() != null){

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        }




        TextView whatsThisTextView = (TextView) findViewById(R.id.whatsThisTextView);

        String underLinedData="What's This?";
        SpannableString content = new SpannableString(underLinedData);
        content.setSpan(new UnderlineSpan(), 0, underLinedData.length(), 0);
        whatsThisTextView.setText(content);




        multiplyByRminTextView = (TextView) findViewById(R.id.mutiplyByRminTextView);
        multiplyByRminTextView.setVisibility(View.INVISIBLE);

        feedFlowRateEditText = (EditText) findViewById(R.id.feedFlowRateEditText);
        FeedFracLightEditText = (EditText) findViewById(R.id.FeedFracLightEditText);
        TopProdFracLightEditText = (EditText) findViewById(R.id.TopProdFracLightEditText);
        BottomProdFracLightEditText = (EditText) findViewById(R.id.BottomProdFracLightEditText);
        RefluxRatioEditText = (EditText) findViewById(R.id.RefluxRatioEditText);
        qLineEditText = (EditText) findViewById(R.id.qLineEditText);

        //initializing the minimum reflux switch
        minRefluxSwitch = (Switch) findViewById(R.id.minRefluxSwitch);

        //initializing the minimum reflux switch state
        minRefluxSwitchState = false;

        calculateButton = (Button) findViewById(R.id.calculateButton);



        //setting the value of the edit texts so that the user doesn't loose their data when thy hit the back button - it resets when they select a new compound in the Search Activity
        feedFlowRateEditText.setText(SearchActivity.sharedPreferences.getString("feedRate","" ));
        FeedFracLightEditText.setText(SearchActivity.sharedPreferences.getString("feedFracLight", ""));
        TopProdFracLightEditText.setText(SearchActivity.sharedPreferences.getString("topProdFracLight", ""));
        BottomProdFracLightEditText.setText(SearchActivity.sharedPreferences.getString("bottomProdFracLight", ""));
        RefluxRatioEditText.setText(SearchActivity.sharedPreferences.getString("refluxRatio", ""));
        qLineEditText.setText(SearchActivity.sharedPreferences.getString("qLineNumber", ""));

        //feed flow rate spinner
        feedFlowRateSpinner = (Spinner) findViewById(R.id.feedFlowRateSpinner);
        ArrayAdapter flowRateAdapter = ArrayAdapter.createFromResource(this, R.array.flowRateUnits, R.layout.support_simple_spinner_dropdown_item);
        feedFlowRateSpinner.setAdapter(flowRateAdapter);

        //feed fraction light component spinner
        FeedFracLightSpinner = (Spinner) findViewById(R.id.FeedFracLightSpinner);
        ArrayAdapter feedFracLightAdapter = ArrayAdapter.createFromResource(this, R.array.fractionUnits, R.layout.support_simple_spinner_dropdown_item);
        FeedFracLightSpinner.setAdapter(feedFracLightAdapter);

        //Top light product fraction units spinner
        TopProdFracLightSpinner = (Spinner) findViewById(R.id.TopProdFracLightSpinner);
        ArrayAdapter TopProdFracLightAdapter = ArrayAdapter.createFromResource(this, R.array.fractionUnits, R.layout.support_simple_spinner_dropdown_item);
        TopProdFracLightSpinner.setAdapter(TopProdFracLightAdapter);

        //Bottom Light Product fraction units Spinner
        BottomProdFracLightSpinner = (Spinner) findViewById(R.id.BottomProdFracLightSpinner);
        ArrayAdapter BottomProdFracLightAdapter = ArrayAdapter.createFromResource(this, R.array.fractionUnits, R.layout.support_simple_spinner_dropdown_item);
        BottomProdFracLightSpinner.setAdapter(BottomProdFracLightAdapter);


        //this is a switch change listener that disables the reflux ratio edit text and turns the reflux ration text view grey when checked
        minRefluxSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (compoundButton.isChecked()) {
                    multiplyByRminTextView.setVisibility(View.VISIBLE);
                } else {
                    multiplyByRminTextView.setVisibility(View.INVISIBLE);
                }

            }
        });


        feedFlowRateEditText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    calculateResults(qLineEditText);
                    return true;
                }

                return false;
            }
        });



        FeedFracLightEditText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    calculateResults(qLineEditText);
                    return true;
                }

                return false;
            }
        });


        TopProdFracLightEditText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    calculateResults(qLineEditText);
                    return true;
                }

                return false;
            }
        });



        BottomProdFracLightEditText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    calculateResults(qLineEditText);
                    return true;
                }

                return false;
            }
        });


        RefluxRatioEditText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    calculateResults(qLineEditText);
                    return true;
                }

                return false;
            }
        });

        qLineEditText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    calculateResults(qLineEditText);
                    return true;
                }

                return false;
            }
        });






    }








}
