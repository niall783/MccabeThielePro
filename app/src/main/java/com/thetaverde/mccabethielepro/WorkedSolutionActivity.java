package com.thetaverde.mccabethielepro;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.StandaloneActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class WorkedSolutionActivity extends AppCompatActivity {


    MathJaxWebView mathJaxWebView;

    int dp = 2; // number of decimal places

    //for the mass balance
    String feedFlowRate = MainActivity.feedFlowRate_Static;
    String feedFrac = ResultsActivity.xf;
    String distFlowRate = "40000";
    String distFrac = ResultsActivity.xd;
    String botFlowRate = "20000";
    String botFrac = ResultsActivity.xb;
    String flowRateUnits = " " + ResultsActivity.units_flowRate;

    Double feedFlowRate_Double = Double.parseDouble(feedFlowRate);
    Double botFract_Double = Double.parseDouble(botFrac);
    Double feedfrac_double = Double.parseDouble(feedFrac);
    Double distFrac_double = Double.parseDouble(distFrac);


    String rminTOLIntersectionPoint = GraphViewActivity.yIntercept_static; //y - coordinate of the intersection point between the y axis and the Top Operating line for the Rmin graph
    String q = ResultsActivity.q;
    String refluxRatio = ResultsActivity.RefluxRatio;
    String stages = ResultsActivity.noOfStages;


    Double rminTOIntersectionPoint_Double = Double.parseDouble(rminTOLIntersectionPoint);


    MainActivity formatNumbers = new MainActivity();


    String tex =
            "Determining the flow rate of the all the streams:" +
            "$$$$F = Feed Flow Rate" +
            "$$$$D = Distillate Flow Rate" +
            "$$$$B = Bottoms Flow Rate" +
            "$$$$xf = Fraction of light Component in the Feed" +
            "$$$$xd = Fraction of light Component in the Distillate" +
            "$$$$xb = Fraction of light Component in the Bottoms" +
            "$$F = D + B$$" +
            "$$"+feedFlowRate+"= D+B$$" +
            "$$$$" +
            "$$F(xf) = D(xd)+B(xb)$$" +
            "$$"+feedFlowRate + "("+feedFrac+")"+ "=D" + "("+distFrac+")"+ "+B"+ "("+botFrac+")"+"$$" +
            "$$$$" +
            "$$F-D = B$$" +
            "$$$$"+
            "$$"+feedFlowRate + "("+feedFrac+")"+ "=D" + "("+distFrac+")"+ "+(F-D)"+ "("+botFrac+")"+"$$"+
            "$$"+feedFlowRate + "("+feedFrac+")"+ "=D" + "("+distFrac+")"+ "+"+"("+feedFlowRate+"-D)"+ "("+botFrac+")"+"$$"+
            "$$"+String.valueOf(formatNumbers.round(feedFlowRate_Double*feedfrac_double,dp))+ "=D" + "("+distFrac+")"+ "+"+String.valueOf(formatNumbers.round(botFract_Double*feedFlowRate_Double,dp))+ "-(D)("+botFrac+")$$"+
            "$$"+String.valueOf(formatNumbers.round(feedFlowRate_Double*feedfrac_double,dp)) + "-" + String.valueOf(formatNumbers.round(botFract_Double*feedFlowRate_Double,dp))+"=D" + "("+distFrac+")"+ "-(D)("+botFrac+")$$"+
            "$$"+String.valueOf(formatNumbers.round(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double)),dp)) +"=D(" +String.valueOf(formatNumbers.round(distFrac_double-botFract_Double,dp))+ ")$$"+
            "$$\\frac{"+String.valueOf(formatNumbers.round(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double)),dp))+"}{" +String.valueOf(formatNumbers.round(distFrac_double-botFract_Double,dp))+ "}"+"\\ =D$$"+
            "$$"+String.valueOf(formatNumbers.round((((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double)))/(distFrac_double-botFract_Double),dp))+"=D$$" +
            "$$D="+String.valueOf(formatNumbers.round(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double))/(distFrac_double-botFract_Double),dp))+" "+flowRateUnits+"$$" +
            "$$$$"+
            "Finding the Bottoms flow rate"+
            "$$F = D + B$$" +
            "$$F - D = B$$"  +
            "$$B = F - D$$" +
            "$$B ="+feedFlowRate + "-"+ String.valueOf(formatNumbers.round(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double))/(distFrac_double-botFract_Double),dp)) +"$$"   +
            "$$B = "+String.valueOf(formatNumbers.round(feedFlowRate_Double-(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double))/(distFrac_double-botFract_Double)),dp))+" "+flowRateUnits+"$$" +
            "$$$$" +
            "Flow Rate of the Light Component in the Feed can be found using: " +
            "$$ Fv = F(xf) $$"+
            "$$Fv ="+String.valueOf(formatNumbers.round(feedFlowRate_Double*feedfrac_double,dp))+ flowRateUnits +" $$" +
            "Flow Rate of the Light Component in the Distillate can be found using: " +
            "$$ Dv = D(xd) $$"+
            "$$Dv ="+String.valueOf(formatNumbers.round(distFrac_double*(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double))/(distFrac_double-botFract_Double)),dp))+flowRateUnits + " $$" +
            "Flow Rate of the Light Component in the Bottoms can be found using: " +
            "$$ Bv = B(xb) $$"+
            "$$Bv ="+String.valueOf(formatNumbers.round(botFract_Double*(((feedFlowRate_Double*feedfrac_double)-(botFract_Double*feedFlowRate_Double))/(distFrac_double-botFract_Double)),dp))+ flowRateUnits +" $$" +
            "$$$$"+
            "$$$$" +
            "$$$$" +
            "Determining the Minimum Reflux Ratio:"+
            "$$$$"+
            "When the Reflux Ratio is equal to the Minimum Reflux Ratio, all the operating lines cross a a point on the Equilibrium Curve."        +
            "$$$$"+
            "Step 1: Plot the Equilibrium Curve. "+
            "$$$$"+
            "Step 2: Plot the 45 degree line i.e. Plot a line that goes through the coordinates (0,0) and (1,1)."+
            "$$$$"+
            "Step 3: Plot the q-line."+
            "$$$$"+
            "Equation for the q-line in shown below."   +
            "$$\\ y = \\frac{-q}{(1-q)}x \\ + \\frac{1}{(1-q)}xf $$"  +  //qline equation
            "$$\\ y = \\frac{-"+q+ "}{(1-" +q+")}x \\ +\\frac{1}{(1-"+q+")}xf$$"  +
            "The first point on the q-line is at the coordinate (xf,xf) and this point resides on the 45 degree line."+
            "$$$$"+
            "Notice where the q-line Intersects the Equilibrium Curve."+
            "$$$$"+
            "Step 4: Plot the Bottom Operating line by drawing a line from the coordinate (xb,0) to the point of intersection between the q-line and Equilibrium Curve."+
            "$$$$"+
            "Step 5: Plot the Top operating line by drawing a line from the coordinate (xd,0) to the point of intersection between the q-line and the Equilibrium Curve. "+
            "Ensure that your extend your Top Operating Line so that it intersects with the y-axis."+
            "$$$$"+
            "A graph with these line drawn can be found by tapping the 'Show Rmin Graph' button at the bottom of the Results Page."+
            "$$$$"+
            "The Equation for the Top Operating Line can found below:"+
            "$$$$"+
            "R = Reflux Ratio"+
            "$$\\ y = \\frac{R}{R+1}x \\ + \\frac{1}{R+1}xd $$"+ //Top Operating Line Equation
            "$$$$"+
            "When the intersection point between the Top Operating line and y-axis is subbed in to the above equation, it becomes:"+
            "$$\\ y = \\frac{1}{Rmin+1}xd$$"+
            "$$$$"+
            "Rearranging to to find Rmin:" +
            "$$\\ Rmin = \\frac{xd}{y}-1$$" +
            "Subbing in the numbers:" +
            "$$\\ Rmin =\\frac{"+distFrac+"}{"+formatNumbers.round(rminTOIntersectionPoint_Double,dp)+"}-1 $$"+
            "Solving for Rmin:"+
            "$$Rmin ="+String.valueOf(formatNumbers.round(((distFrac_double/rminTOIntersectionPoint_Double)-1),dp))+" $$"   + //Rmin final Answer
            "$$$$" +
            "It is important to know how to find the Minimum Reflux Ratio because it is common for exam questions to give the Reflux Ratio as a function of the Minimum Reflux Ratio."  +
            "$$$$" +
            "For Example:"+
            "$$R = 2.5(Rmin)$$"+
            "$$$$"+
            "$$$$" +
            "$$$$" +
            "$$$$"+
            "$$$$"+
            "Finding the number of stages:"+
            "$$$$"+
            "Step 1: Plot the Equilibrium Curve."+
            "$$$$" +
            "Step 2: Plot the 45 degree line i.e. Plot a line that goes through the coordinates (0,0) and (1,1)."+
            "$$$$"+
            "Step 3: Plot the q-line." +
            "$$$$"+
            "Equation for the q-line in shown below."   +
            "$$\\ y = \\frac{-q}{(1-q)}x \\ + \\frac{1}{(1-q)}xf $$"  +  //qline equation
            "$$\\ y = \\frac{-"+q+ "}{(1-" +q+")}x \\ +\\frac{1}{(1-"+q+")}xf$$"  +
            "The first point on the q-line is at the coordinate (xf,xf) and this point resides on the 45 degree line."+
            "$$$$"  +
            "Step 4: Plot the Top Operating Line using the Equation below."+
            "$$\\ y = \\frac{R}{R+1}x \\ + \\frac{1}{R+1}xd $$"+ //Top Operating Line Equation
            "Subbing in the numbers:"+
            "$$\\ y = \\frac{"+refluxRatio+ "}{"+ refluxRatio +"+1}x \\ + \\frac{1}{"+refluxRatio+"+1}"+distFrac +"$$"+ //Top Operating Line Equation
            "Notice the point were the Top Operating Line crosses the q-line." +
            "$$$$"+
            "Step 5: Plot the Bottom Operating line." +
            "$$$$"+
            "In order to plot the Bottom Operating line, draw a line from the coordinate (xb,0) to the point were the Top Operating Line intersects the q-line."  +
            "$$$$" +
            "Step 6: Plotting Stages."+
            "$$$$"+
            "To plot the first stage, draw a horizontal line between the coordinate (xd,xd) and the Equilibrium Curve. Now draw a vertical line straight down until you inersect the Top Operating Line."+
            "$$$$"+
            "Continue to repeat this process until one of your horizontal lines horizontally passes the point of intersection between the operating lines and the q-line." +
            "$$$$"  +
            "After passing this point, instead of drawing a vertical line from the Equlibrium Curve down to the top operating line. The vertical line is now drawn down until it intersects the Bottom operating line."+
            "$$$$"+
            "Continue drawing stages until you horizontally pass xb on the x-axis."+
            "$$$$"+
            "The number of Stages is equal number of full triangles draw or it is also equal to the number of lines drawn divided by 2."+
            "$$$$" +
            "In this case the number of Stages is = "+ stages + " Stages"+
            "$$$$"+
            "A graph showing all the operating lines and stages can be found at the Bottom of the Results Page by tapping the Button 'Show Stages Graph'. "
            ;


//not used - cannot activate the intend to change to the graph activity
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.worked_solution_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()){

            case R.id.show_stages_graph_worked_sol:
                 startActivity(ResultsActivity.stagesGraphIntent);
                return true;
            case R.id.show_rmin_graph_workd_sol:
                 startActivity(ResultsActivity.rminGraphIntent);
                return true;
            default: return false;
        }


    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //NavUtils.navigateUpFromSameTask(this);
            startActivity(MainActivity.intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worked_solution);

        setTitle("Solution");

        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }


        mathJaxWebView =(MathJaxWebView)findViewById(R.id.webView);
        mathJaxWebView.getSettings().setJavaScriptEnabled(true);
        mathJaxWebView.setText(tex);



    }
}
