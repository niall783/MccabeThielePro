package com.thetaverde.mccabethielepro;

/**
 * Created by niall_000 on 13/07/2017.
 */

public class PolynomialCheck {



    public Double[] generateFortyFiveDegreeLinePoints(){

        //for populating the fortyFiveDegreeLineXValsArray
        int numOfPoints = 1000; //excluding Zero
        Double interval = (1.0 - 0.0) / numOfPoints;

        Double[] fortyFiveDegreeLineXValsArray = new Double[numOfPoints + 1];

        fortyFiveDegreeLineXValsArray[0] = 0.0;

        //populating the fortyFiveDegreeLineXVals with 1001 numbers from 0 to 1
        for (int j = 1; j <= 1000; j++) {
            fortyFiveDegreeLineXValsArray[j] = fortyFiveDegreeLineXValsArray[j - 1] + interval;
        }

        //replacing the last value with an exact value of 1
        fortyFiveDegreeLineXValsArray[fortyFiveDegreeLineXValsArray.length - 1] = 1.0;

        //forty five degree line x and y values are the same
        return fortyFiveDegreeLineXValsArray;

    }

    //this check to ensure that the polynomial that the user has enter doesn't ever dip below the forty five degree line
    public Boolean fortyFiveDegreeLineCheck(Double [] userYVals) {

        Boolean userDataIsGood = true;

        Double[] fortyFiveDegreeLineXValsArray = generateFortyFiveDegreeLinePoints();

        for (int i = 0 ; i < fortyFiveDegreeLineXValsArray.length; i++){


            if(userYVals[i] < fortyFiveDegreeLineXValsArray[i]){

                userDataIsGood = false;

            }

        }


        return userDataIsGood;


    }



}
