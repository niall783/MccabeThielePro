package com.thetaverde.mccabethielepro;

/**
 * Created by niall_000 on 01/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import static com.thetaverde.mccabethielepro.SearchActivity.sharedPreferences;

public class DatabaseHelper extends SQLiteOpenHelper {

    private String DATABASE_PATH = "";

    private static final String DATABASE_NAME = "binarycomponentsdatabase.db";



    private static final String userComponentTable_Name = "USERCOMPONENTS";

    private static final String userCompoundNamesColumn_Name = "userCompoundNames";

    private static  final String userXVALS = "userXVALS";

    private static final String userYVALS = "userYVALS";



    private  static  final String usercomponentnamesTable_Name = "USERCOMPONENTNAMES";

    private static final String humanReadableName = "humanreadablename";

    private static final String sqlName = "sqlname";


    private final Context myContext;

    public SQLiteDatabase db;

    ArrayList<String> humanReadableCompoundNames;
    ArrayList<String> sqlCompoundNames;

    Double[] arrayXVals;
    Double[] arrayYVals;





    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.myContext = context;

        if(android.os.Build.VERSION.SDK_INT >= 17){
            DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
    }




    public void insertDataIntoDataBase(Double[] xvalsArray,Double[] yValsArray,String originalUserComponentsName){

        Log.i("ArrayValueX", Arrays.toString(xvalsArray));
        Log.i("ArrayValueY", Arrays.toString(yValsArray));


        String userComponentsName = originalUserComponentsName;

        //keeping track of the number off times that a particular compound name has been entered by the user
        int nameCount = 1;

        //calling the method to update the human readable names arraylist so i can check to make sure the same name has not been entered twice
        getHumanReadableCompoundNames();

        //checking to see if the same user compound name has been entered more than once
        for (int e = 0 ; e < humanReadableCompoundNames.size(); e ++){
            //if the same compound name has been entered twice then append 1,2,3 etc to it depending on how many of the same name have been entered
            if(userComponentsName.equals(humanReadableCompoundNames.get(e))){

                userComponentsName = originalUserComponentsName + nameCount;

                nameCount = nameCount + 1;
            }
        }


        String XvalsColumnName =  userXVALS;
        String YvalsColumnName =  userYVALS;



        //initializing the content value for the user's array data
        ContentValues userArrayValuesContentValues = new ContentValues();

            //putting the user's equilibrium data into contentvalues so that they can be added into the sqlite database
            //i have to put the array in as a string because it takes to look to put 1001 individual data points into the database
            userArrayValuesContentValues.put(userCompoundNamesColumn_Name,"USER" + userComponentsName);
            userArrayValuesContentValues.put(XvalsColumnName,Arrays.toString(xvalsArray));
            userArrayValuesContentValues.put(YvalsColumnName,Arrays.toString(yValsArray));


            //putting the content values thing with all the coordinates into the database
            getWritableDatabase().insert(userComponentTable_Name,XvalsColumnName,userArrayValuesContentValues);


            //initializing the content values for the user's compound name
            // both the human readable name that they put in and the sql name that i made up
            ContentValues userComponentNameContentValues = new ContentValues();
            //inserting the human readable name that the user provided and my identifying number into the database
            userComponentNameContentValues.put(humanReadableName,userComponentsName);
            userComponentNameContentValues.put(sqlName,"USER" + userComponentsName);

            getWritableDatabase().insert(usercomponentnamesTable_Name,humanReadableName,userComponentNameContentValues);



    }

    public void deleteFromDatabase(String sqlNameForUserData){


        getWritableDatabase().delete(userComponentTable_Name,userCompoundNamesColumn_Name + " = '" + sqlNameForUserData + "'",null);

        //getWritableDatabase().execSQL("DELETE FROM "+ userComponentTable_Name + " WHERE " + userCompoundNamesColumn_Name + " = " + sqlNameForUserData);

        getWritableDatabase().delete(usercomponentnamesTable_Name,sqlName + " = '"+ sqlNameForUserData + "'" ,null);
    }


    //method that pulls the equilibrium data that the user inputted out of the database
    public void pullUserEquilibriumData(){

        String binaryMixtureName = sharedPreferences.getString("sqlitebinaryMixtureName","");


        String TABLE_NAME = userComponentTable_Name;

        String sqlQueryString = "SELECT * FROM " + TABLE_NAME + " WHERE " + userCompoundNamesColumn_Name + " = '" + binaryMixtureName + "' LIMIT 1";



        String arrayXvals_String = "";
        String arrayYvals_String = "";

        Cursor componentNamesCursor = getReadableDatabase().rawQuery(sqlQueryString,null);

        int compoundNames_INDEX = componentNamesCursor.getColumnIndex(userCompoundNamesColumn_Name);
        int xVals_INDEX = componentNamesCursor.getColumnIndex(userXVALS);
        int yVals_INDEX = componentNamesCursor.getColumnIndex(userYVALS);



        if (componentNamesCursor.getColumnCount() > 0){

            componentNamesCursor.moveToFirst();
            while (!componentNamesCursor.isAfterLast()){


                 arrayXvals_String = componentNamesCursor.getString(xVals_INDEX);
                 arrayYvals_String = componentNamesCursor.getString(yVals_INDEX);

                componentNamesCursor.moveToNext();
            }

        }

        componentNamesCursor.close();


        arrayXvals_String = arrayXvals_String.replace("[","");
        arrayXvals_String = arrayXvals_String.replace("]","");

        arrayYvals_String = arrayYvals_String.replace("[","");
        arrayYvals_String = arrayYvals_String.replace("]","");

        String[] arrayXvals_StringArray = arrayXvals_String.split(",");
        String[] arrayYVals_StringArray = arrayYvals_String.split(",");


        arrayXVals = new Double[arrayXvals_StringArray.length];

        for(int t = 0 ; t < arrayXvals_StringArray.length; t++){
            arrayXVals[t] = Double.valueOf(arrayXvals_StringArray[t]);
            Log.i("ArrayFromStringX", String.valueOf(arrayXVals[t]));
        }

        arrayYVals = new Double[arrayYVals_StringArray.length];

        for (int h = 0 ; h < arrayYVals_StringArray.length; h++) {
            arrayYVals[h] = Double.valueOf(arrayYVals_StringArray[h]);
            Log.i("ArrayFromStringY", String.valueOf(arrayYVals[h]));
        }


    }

    //methods that allow another activity to get the user array value that they entered and saved
    public Double[] getUserArrayXVals(){
        pullUserEquilibriumData();
        return arrayXVals;
    }
    public Double[] getUserArrayYVals(){
        pullUserEquilibriumData();
        return arrayYVals;
    }







    //getting compound names from the sqlite do that they can be displayed in the listview in the search activity
    public void getComponentNames(){

        humanReadableCompoundNames = new ArrayList<>();
        sqlCompoundNames = new ArrayList<>();

        humanReadableCompoundNames.clear();
        sqlCompoundNames.clear();

        humanReadableCompoundNames.add("Enter your own data!");
        sqlCompoundNames.add("notNeeded");

        /*String TABLE_NAME = "COMPONENTNAMES";
        String sqlQueryString = "SELECT * FROM " + TABLE_NAME;

        Cursor componentNamesCursor = getReadableDatabase().rawQuery(sqlQueryString,null);

        int humanreadablename_INDEX = componentNamesCursor.getColumnIndex("humanreadablename");
        int sqlname_INDEX = componentNamesCursor.getColumnIndex("sqlname");


        if (componentNamesCursor.getColumnCount() > 0){

            componentNamesCursor.moveToFirst();
            while (!componentNamesCursor.isAfterLast()){

                humanReadableCompoundNames.add(componentNamesCursor.getString(humanreadablename_INDEX));

                sqlCompoundNames.add(componentNamesCursor.getString(sqlname_INDEX));

                componentNamesCursor.moveToNext();
            }


        }

        componentNamesCursor.close();*/


    }


    //getting compound names from the sqlite do that they can be displayed in the listview in the search activity
    public void getUserComponentNames(){


        String TABLE_NAME = usercomponentnamesTable_Name;
        String sqlQueryString = "SELECT * FROM " + TABLE_NAME;

        Cursor componentNamesCursor = getReadableDatabase().rawQuery(sqlQueryString,null);

        int humanreadablename_INDEX = componentNamesCursor.getColumnIndex(humanReadableName);
        int sqlname_INDEX = componentNamesCursor.getColumnIndex(sqlName);


        if (componentNamesCursor.getColumnCount() > 0){

            componentNamesCursor.moveToFirst();
            while (!componentNamesCursor.isAfterLast()){

                humanReadableCompoundNames.add(componentNamesCursor.getString(humanreadablename_INDEX));

                sqlCompoundNames.add(componentNamesCursor.getString(sqlname_INDEX));

                componentNamesCursor.moveToNext();
            }


        }

        componentNamesCursor.close();

       // humanReadableCompoundNames.add("More compounds coming soon!");
        //sqlCompoundNames.add("notNeeded");
    }






    //a method that can be called by another activity to get the human readable compound names
    public ArrayList<String> getHumanReadableCompoundNames(){
        getComponentNames(); //filling up half off the humanReadableCompoundNames array list with the compound names that come with th app
        getUserComponentNames(); //and the other half with the compounds that the user has entered
        return humanReadableCompoundNames;
    }

    public ArrayList<String> getSqlCompoundNames(){
        getComponentNames();
        getUserComponentNames();
        return sqlCompoundNames;
    }




    //method used to pull the data that comes with the app out of the database
    public void pullStandardEquilibriumData(){

      //BinaryComponentsDTO binaryComponentsDTO = new BinaryComponentsDTO();

        String TABLE_NAME = "COMPONENTS";
        String binaryMixtureName = sharedPreferences.getString("sqlitebinaryMixtureName","");

        Log.i("binaryMixtureName",binaryMixtureName);


        // String sqlQueryStatement = "SELECT * FROM " + TABLE_NAME + " WHERE " + COMPOUNDNAMES + " = " + "'" + compoundName + "'";
        String sqlQueryStatement = "SELECT * FROM " + TABLE_NAME;


        Cursor cursor = getReadableDatabase().rawQuery(sqlQueryStatement,null);

        //storing the column indexes as variables
        //int cacheIDIndex = cursor.getColumnIndex(CACHE_ID);
        int xvalsIndex = cursor.getColumnIndex(binaryMixtureName + "XVALS");
        int yvalsIndex = cursor.getColumnIndex(binaryMixtureName + "YVALS");



        ArrayList<Double>  xvalsArrayList = new ArrayList<>();
        ArrayList<Double>  yvalsArrayList = new ArrayList<>();

        //did we get results
        if (cursor.getColumnCount() > 0){

            //move to the first result
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){

                xvalsArrayList.add(cursor.getDouble(xvalsIndex));
                yvalsArrayList.add(cursor.getDouble(yvalsIndex));


                cursor.moveToNext();
            }

        }

        cursor.close();


        arrayXVals = new Double[xvalsArrayList.size()];
        arrayXVals =  xvalsArrayList.toArray(arrayXVals);

        arrayYVals = new Double[yvalsArrayList.size()];
        arrayYVals = yvalsArrayList.toArray(arrayYVals);

        //Log.i("ArrayValsx",xvalsArrayList.toString());
        //Log.i("ArrayValsy",yvalsArrayList.toString());

    }

    //methods to return the array xVals and yVals that come standard with the app i.e. not data that the user has inputted and saved
    public Double[] getStandardArrayXVals(){
        pullStandardEquilibriumData();
        return arrayXVals;
    }
    public Double[] getStandardArrayYVals(){
        pullStandardEquilibriumData();
        return arrayYVals;
    }






    //method used to create the user component table
    public void createUserComponentTable(){

        String sqlCreateTableStatement = "CREATE TABLE IF NOT EXISTS " + userComponentTable_Name + " ( id INTEGER PRIMARY KEY AUTOINCREMENT,"+ userCompoundNamesColumn_Name +  " TEXT,"  + userXVALS + " TEXT," + userYVALS + " TEXT)";

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        sqLiteDatabase.execSQL(sqlCreateTableStatement);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }



    public void createDatabase() {
        createDB();
    }

    private void createDB() {
        boolean dbExist = DBExists();

        if(!dbExist) {
            this.getReadableDatabase();
            copyDBFromResource();
        }
    }

    private boolean DBExists() {
        SQLiteDatabase db = null;

        try {
            String databasePath = DATABASE_PATH + DATABASE_NAME;
            db = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE);
            db.setLocale(Locale.getDefault());
            db.setLockingEnabled(true);
            db.setVersion(1);
        } catch (SQLiteException e) {
            Log.e("SqlHelper", "database not found");
        }

        if (db != null) {
            db.close();
        }
        return db != null ? true : false;
    }

    private void copyDBFromResource() {
        InputStream inputStream = null;
        OutputStream outStream = null;
        String dbFilePath = DATABASE_PATH + DATABASE_NAME;

        try {
            inputStream = myContext.getAssets().open(DATABASE_NAME);
            outStream = new FileOutputStream(dbFilePath);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            outStream.flush();
            outStream.close();
            inputStream.close();

            //method used to create the user component table - my own code
            createUserComponentTable();

            Log.i("DatabaseCopied","Good Job");

        } catch (IOException e) {
            throw new Error("Problem copying database from resource file.");
        }

    }

}
