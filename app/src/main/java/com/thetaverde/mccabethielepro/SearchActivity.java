package com.thetaverde.mccabethielepro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {

    FloatingActionButton floatingActionButton;

    ArrayAdapter arrayAdapter;

    ListView searchListView;

    ArrayList<String> humanReadableCompoundNames;
    ArrayList<String> sqliteCompoundsName;

    String newTextString;

    static SharedPreferences sharedPreferences;

    static Double[] arrayXvals;
    static Double[] arrayYvals;

    Boolean isItemNotDeleted = true;


    //search box in the action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);

        MenuItem Item = menu.findItem(R.id.menuSearch);

        SearchView searchView = (SearchView) Item.getActionView();

        searchView.setQueryHint("Search...");

        searchView.setMaxWidth(Integer.MAX_VALUE);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                humanReadableCompoundNames.add(0,"Enter your own data!");

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                //

                arrayAdapter.getFilter().filter(newText);

                newTextString = newText;


                return false;
            }
        });





        return super.onCreateOptionsMenu(menu);
    }


    //method that populates the the list view with item stored in the sqlite database
    public void populateListView(){


        //initializing the the databaseHelper object
        final DatabaseHelper databaseHelper = new DatabaseHelper(this);

        //creating database if it doesn't already exist
        databaseHelper.createDatabase();

        //initializing shared preferences to save the current user selected item,.
        sharedPreferences = SearchActivity.this.getSharedPreferences("com.niall001.mccabethiele", Context.MODE_PRIVATE);



        //initializing the listView
        searchListView = (ListView) findViewById(R.id.searchListView);


        //setting the data of the compound names from the sql database to a local array list in this activity
        humanReadableCompoundNames = databaseHelper.getHumanReadableCompoundNames();
        sqliteCompoundsName = databaseHelper.getSqlCompoundNames();



        arrayAdapter = new ArrayAdapter<>(SearchActivity.this,android.R.layout.simple_list_item_1,databaseHelper.getHumanReadableCompoundNames());


        searchListView.setAdapter(arrayAdapter);




    }




    @Override
    protected void onResume() {
        super.onResume();

    populateListView();

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setTitle("Equilibrium Data");


        populateListView();




        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addNewEquilibriumDate();


            }
        });


        /*searchListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(SearchActivity.this,"OK!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/



        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (isItemNotDeleted) {
                    //if the selected item is not "Enter your own data" then proceed tot the main activity
                    if (!adapterView.getItemAtPosition(i).equals("Enter your own data!") && !adapterView.getItemAtPosition(i).equals("More compounds coming soon!")) {

                        //resetting the graph Title and axis Labels when a new compound is selected
                        SearchActivity.sharedPreferences.edit().putString("graphTitle", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("xAxisLabel", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("yAxisLabel", "").apply();

                        SearchActivity.sharedPreferences.edit().putString("feedRate", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("feedFracLight", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("topProdFracLight", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("bottomProdFracLight", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("refluxRatio", "").apply();
                        SearchActivity.sharedPreferences.edit().putString("qLineNumber", "").apply();


                        //saving the corresponding SqliteBinaryMixtureName in shared preferences
                        for (int p = 0; p < humanReadableCompoundNames.size(); p++) {

                            if (humanReadableCompoundNames.get(p).equals(String.valueOf(adapterView.getItemAtPosition(i)))) {
                                sharedPreferences.edit().putString("sqlitebinaryMixtureName", sqliteCompoundsName.get(p)).apply();

                            }

                        }

                        //initializing the data base helper object
                        DatabaseHelper databaseHelper = new DatabaseHelper(SearchActivity.this);

                        if (!sharedPreferences.getString("sqlitebinaryMixtureName", "").substring(0, 4).equals("USER")) {
                            //get the equilibrium array from the sqlite database
                            Log.i("StringTest", sharedPreferences.getString("sqlitebinaryMixtureName", "").substring(0, 4));
                            arrayXvals = databaseHelper.getStandardArrayXVals();
                            arrayYvals = databaseHelper.getStandardArrayYVals();
                        } else {
                            arrayXvals = databaseHelper.getUserArrayXVals();
                            arrayYvals = databaseHelper.getUserArrayYVals();
                        }


                        //staring the Main Activity when an item from the list View is tapped
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);


                    } else if (adapterView.getItemAtPosition(i).equals("Enter your own data!")) {

                        addNewEquilibriumDate();



                    } else if (adapterView.getItemAtPosition(i).equals("More compounds coming soon!")) {
                        Toast.makeText(getApplicationContext(), "Be Patient! :)", Toast.LENGTH_SHORT).show();
                    }


                }else{
                    isItemNotDeleted = true;
                }
            }



        });


        searchListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                isItemNotDeleted = false;

                //saving the corresponding SqliteBinaryMixtureName in shared preferences
                for (int p = 0 ; p < humanReadableCompoundNames.size();p++){

                    if (humanReadableCompoundNames.get(p).equals(String.valueOf(adapterView.getItemAtPosition(i)))){
                        sharedPreferences.edit().putString("sqlitebinaryMixtureName", sqliteCompoundsName.get(p)).apply();

                    }

                }



                if (!sharedPreferences.getString("sqlitebinaryMixtureName","").substring(0,4).equals("USER")) {

                    Toast.makeText(SearchActivity.this,"You cannot delete this data!",Toast.LENGTH_SHORT).show();

                }else {

                    openDeleteDataDialogueBox();



                }




                return false;
            }
        });





    }

    public void addNewEquilibriumDate(){

        Intent intent = new Intent(getApplicationContext(), customEquilibriumDataActivity.class);
        startActivity(intent);

        //resetting the graph Title and axis Labels when a new compound is selected
        SearchActivity.sharedPreferences.edit().putString("graphTitle", "").apply();
        SearchActivity.sharedPreferences.edit().putString("xAxisLabel", "").apply();
        SearchActivity.sharedPreferences.edit().putString("yAxisLabel", "").apply();

        SearchActivity.sharedPreferences.edit().putString("feedRate", "").apply();
        SearchActivity.sharedPreferences.edit().putString("feedFracLight", "").apply();
        SearchActivity.sharedPreferences.edit().putString("topProdFracLight", "").apply();
        SearchActivity.sharedPreferences.edit().putString("bottomProdFracLight", "").apply();
        SearchActivity.sharedPreferences.edit().putString("refluxRatio", "").apply();
        SearchActivity.sharedPreferences.edit().putString("qLineNumber", "").apply();

    }



    public void openDeleteDataDialogueBox(){

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(SearchActivity.this);
        alertDialog.setTitle("Delete Data");
        alertDialog.setMessage("Are you sure?");







        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        DatabaseHelper databaseHelper = new DatabaseHelper(SearchActivity.this);

                        databaseHelper.deleteFromDatabase(sharedPreferences.getString("sqlitebinaryMixtureName",""));


                        populateListView();

                        Toast.makeText(SearchActivity.this,"Item Deleted!",Toast.LENGTH_SHORT).show();

                    }
                });




        alertDialog.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertDialog.show();
    }




}
