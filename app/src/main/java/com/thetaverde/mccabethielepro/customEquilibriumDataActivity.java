package com.thetaverde.mccabethielepro;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.jjoe64.graphview.series.DataPoint;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

import java.util.ArrayList;
import java.util.Arrays;

import static android.R.attr.password;
import static android.R.attr.x;

public class customEquilibriumDataActivity extends AppCompatActivity {



    int numOfPoints = 0;

    ConstraintLayout constraintLayout;

    ArrayList<Integer> xEditTextsId;
    ArrayList<Integer> yEditTextsId;

    ArrayList<Double> userXCoords_temp;
    ArrayList<Double> userYCoords_temp;

    ArrayList<Double> userXCoords;
    ArrayList<Double> userYCoords;


    //method that clears all the entered data points
    public void clearAll(View view){

        for(int j = 0 ; j < xEditTextsId.size();j++) {

            EditText xValEditText = (EditText) findViewById(xEditTextsId.get(j));
            EditText yValEditText = (EditText) findViewById(yEditTextsId.get(j));

            xValEditText.setText("");
            yValEditText.setText("");
        }

    }















    //method that takes the users entered data and passes it to the appropriate location
    public void submitData(View view){

            //pullDataFromEditTexts();

        Log.i("BooleanValue",pullDataFromEditTexts().toString());

        if(xEditTextsId.size() >= 3 && pullDataFromEditTexts()) {


                //for populating the userXValsArray
                int numOfPoints = 1000; //excluding Zero
                Double interval = (1.0 - 0.0) / numOfPoints;

                Double[] userXValsArray = new Double[numOfPoints + 1];
                Double[] userYValsArray = new Double[numOfPoints + 1];


                // Collect data.
                final WeightedObservedPoints obs = new WeightedObservedPoints();
                //populating the obs list with the list of user entered coordinates
                for (int i = 0; i < userXCoords.size(); i++) {

                    obs.add(userXCoords.get(i),userYCoords.get(i));

                }


// Instantiate a n-degree polynomial fitter.
                int order = 6; //polynomial order i.e. 5th order or 6th order for example
                final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(order);



// Retrieve fitted parameters (coefficients of the polynomial function).
                final double[] coeff = fitter.fit(obs.toList());



                userXValsArray[0] = 0.0;
                //userYValsArray[0] = 0.0;



                //populating the userXVals with 1001 numbers from 0 to 1
                for (int j = 1; j <= 1000; j++) {
                    userXValsArray[j] = userXValsArray[j - 1] + interval;
                }

                //replacing the last value with an exact value of 1
                userXValsArray[userXValsArray.length - 1] = 1.0;


                int len = coeff.length;

                for (int k = 0; k < userXValsArray.length; k++) {
                    //equation for the y values
                    userYValsArray[k] = coeff[len - 1] * Math.pow(userXValsArray[k], order) +
                            coeff[len - 2] * Math.pow(userXValsArray[k], order - 1) +
                            coeff[len - 3] * Math.pow(userXValsArray[k], order - 2) +
                            coeff[len - 4] * Math.pow(userXValsArray[k], order - 3) +
                            coeff[len - 5] * Math.pow(userXValsArray[k], order - 4) +
                            coeff[len - 6] * Math.pow(userXValsArray[k], order - 5) +
                            coeff[len - 7] * Math.pow(userXValsArray[k], order - order);

                    Log.i("YVals", String.valueOf(userYValsArray[k]));


                }
                //replacing the first and last values in the y Vals array with 0 and 1
                userYValsArray[0] = 0.0;
                userYValsArray[userYValsArray.length - 1] = 1.0;




                SearchActivity.arrayXvals = userXValsArray;
                SearchActivity.arrayYvals = userYValsArray;
                //Log.i("ArrayValuesX",Arrays.toString(SearchActivity.arrayXvals));
                //Log.i("ArrayValuesY",Arrays.toString(SearchActivity.arrayYvals));

                //DatabaseHelper databaseHelper = new DatabaseHelper(this);

                //databaseHelper.insertDataIntoDataBase(userXValsArray,userYValsArray,"GoodJobNiall");




               // Intent intent = new Intent(this, MainActivity.class);
                //startActivity(intent);

            PolynomialCheck polynomialCheck = new PolynomialCheck();

            if(polynomialCheck.fortyFiveDegreeLineCheck(userYValsArray)) {
                openSaveDataDialogueBox(userXValsArray, userYValsArray);
            }else {

                Toast.makeText(this,"Ooops, check your data again!", Toast.LENGTH_SHORT).show();
                Toast.makeText(this,"Ooops, check your data again!", Toast.LENGTH_SHORT).show();

            }


        }



    }




    public void generateCoordinateEditTexts(int numberOfCoordinates){

        xEditTextsId.clear();
        yEditTextsId.clear();

        userXCoords.clear();
        userYCoords.clear();



        ConstraintSet set = new ConstraintSet();
if (numberOfCoordinates != 0) {

    for (int i = 1; i <= numberOfCoordinates; i++) {

        String xEditTextTag = "xEditText" + i;
        String yEditTextTag = "yEditText" + i;


        EditText xEditText = new EditText(this);
        xEditText.setId(View.generateViewId());
        xEditText.setTag(xEditTextTag);
        xEditText.setHint("x" + i);
        xEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        xEditTextsId.add(xEditText.getId());


        EditText yEditText = new EditText(this);
        yEditText.setId(View.generateViewId());
        yEditText.setTag(yEditTextTag);
        yEditText.setHint("y" + i);
        yEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        yEditTextsId.add(yEditText.getId());


        constraintLayout.addView(yEditText.findViewWithTag(yEditTextTag));

        constraintLayout.addView(xEditText.findViewWithTag(xEditTextTag));

        set.clone(constraintLayout);

        set.create(R.id.centreGuidelineNested, ConstraintSet.VERTICAL_GUIDELINE);
        set.setGuidelinePercent(R.id.centreGuidelineNested, (float) 0.5);

        if (i == 1) {

            set.connect(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 8);
            set.connect(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 8);
            set.connect(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.END, R.id.centreGuidelineNested, ConstraintSet.END, 8);
            set.constrainWidth(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.MATCH_CONSTRAINT);


            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 8);
            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.START, R.id.centreGuidelineNested, ConstraintSet.START, 8);
            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 8);
            set.constrainWidth(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.MATCH_CONSTRAINT);

            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.BASELINE, xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.BASELINE);
        } else {


            set.connect(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.TOP, xEditTextsId.get(i - 2), ConstraintSet.BOTTOM, 8);
            set.connect(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 8);
            set.connect(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.END, R.id.centreGuidelineNested, ConstraintSet.START, 8);
            set.constrainWidth(xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.MATCH_CONSTRAINT);


            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.TOP, yEditTextsId.get(i - 2), ConstraintSet.BOTTOM, 8);
            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.START, R.id.centreGuidelineNested, ConstraintSet.END, 8);
            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 8);
            set.constrainWidth(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.MATCH_CONSTRAINT);

            set.connect(yEditText.findViewWithTag(yEditTextTag).getId(), ConstraintSet.BASELINE, xEditText.findViewWithTag(xEditTextTag).getId(), ConstraintSet.BASELINE);


        }


        set.applyTo(constraintLayout);


    }

}




    }








    //method to check if a number edit text is actually a number
    public Boolean checkNumEditText(String editTextString) {

        if (editTextString.equals("") ||editTextString.equals(".") || editTextString.equals(",") || editTextString.equals("-") || editTextString.equals("0.")) {
            return true;
        } else {
            return false;
        }

    }

    //method checking that the range of the number entered is between zero and one
    public Boolean rangeZeroToOneCheck(String editTextString) {

        Double editTextDouble = Double.parseDouble(editTextString);

        if (editTextDouble > 1 || editTextDouble < 0) {
            return true;
        } else {

            return false;
        }


    }


    public void removeCoordinateEditTexts(){

        //loops through all the editTexts and removes them
        //could use .removeAllView   but that also removes the guideline
        //added the guideline back in programmatically so either will work
        for (int t = 0; t <xEditTextsId.size();t++){

            EditText xValEditText_Temp = (EditText) findViewById(xEditTextsId.get(t));
            //constraintLayout.removeAllViews();
            constraintLayout.removeView(xValEditText_Temp);

            EditText yValEditText_Temp  = (EditText) findViewById(yEditTextsId.get(t));
            //constraintLayout.removeAllViews();
            constraintLayout.removeView(yValEditText_Temp);


        }

    }

    public Boolean pullDataFromEditTexts(){

        userXCoords.clear();
        userYCoords.clear();

        userXCoords_temp.clear();
        userYCoords_temp.clear();

        Boolean userPointsGood = true;
        for(int j = 0 ; j < xEditTextsId.size();j++) {

            EditText xValEditText = (EditText) findViewById(xEditTextsId.get(j));
            EditText yValEditText = (EditText) findViewById(yEditTextsId.get(j));

            //checks to make sure that the user enter an appropriate coordinate
            if(checkNumEditText(xValEditText.getText().toString())) {
                Toast.makeText(this, "Please enter appropriate coordinates.", Toast.LENGTH_SHORT).show();
                userPointsGood = false;
                break;
            }else if( checkNumEditText(yValEditText.getText().toString())){
                Toast.makeText(this, "Please enter appropriate coordinates.", Toast.LENGTH_SHORT).show();
                userPointsGood = false;
                break;
            }else if(rangeZeroToOneCheck(yValEditText.getText().toString())){
                Toast.makeText(this, "All points must be between 0 snd 1", Toast.LENGTH_SHORT).show();
                userPointsGood = false;
                break;
            }else if(rangeZeroToOneCheck(xValEditText.getText().toString())) {
                Toast.makeText(this, "All points must be between 0 snd 1", Toast.LENGTH_SHORT).show();
                userPointsGood = false;
                break;
            }else{
                userXCoords_temp.add(Double.valueOf(xValEditText.getText().toString()));
                userYCoords_temp.add(Double.valueOf(yValEditText.getText().toString()));
            }
        }

        //ensuring that the first value of the coordinates is (0,0)
        userXCoords.add(0.0);
        userYCoords.add(0.0);
        //populating the user coordinates array with the user coordinates array
        for(int h = 0; h < userYCoords_temp.size(); h++){
            userXCoords.add(userXCoords_temp.get(h));
            userYCoords.add(userYCoords_temp.get(h));
        }
        //ensuring that th last value of the coordinates is (1.0,1.0)
        userXCoords.add(1.0);
        userYCoords.add(1.0);

        return userPointsGood;
    }



    public void openSaveDataDialogueBox(final Double[] userXValsArray, final Double [] userYValsArray){

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(customEquilibriumDataActivity.this);
        alertDialog.setTitle("Save Equilibrium Data");
        alertDialog.setMessage("Name your Data...");


        final EditText input = new EditText(customEquilibriumDataActivity.this);
        input.setHint("e.g. Ethanol and Water");

        //only allowing alphabet characters and white spaces
        input.setFilters(new InputFilter[] {
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if(cs.equals("")){ // for backspace
                            return cs;
                        }
                        if(cs.toString().matches("[a-zA-Z ]+")){
                            return cs;
                        }
                        return "";
                    }
                }
        });



        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        //alertDialog.setIcon(R.drawable.key);




        alertDialog.setPositiveButton("SAVE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (!checkNumEditText(input.getText().toString())) {

                            DatabaseHelper databaseHelper = new DatabaseHelper(customEquilibriumDataActivity.this);

                            databaseHelper.insertDataIntoDataBase(userXValsArray, userYValsArray, input.getText().toString());

                            Toast.makeText(customEquilibriumDataActivity.this, "Equilibrium Data Saved", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(customEquilibriumDataActivity.this, MainActivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(customEquilibriumDataActivity.this,"Please enter an appropriate name",Toast.LENGTH_SHORT).show();
                            openSaveDataDialogueBox(userXValsArray, userYValsArray); //this reopens the dialogue box if the user enter not name or an invalid character

                        }
                    }
                });


        alertDialog.setNeutralButton("SKIP", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(customEquilibriumDataActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });

        alertDialog.setNegativeButton("BACK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertDialog.show();
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // setContentView(R.layout.activity_custom_equilibrium_data);
        setContentView(R.layout.activity_user_equilibrium_data);

        setTitle("Enter Coordinates");

        xEditTextsId = new ArrayList<>();
        yEditTextsId = new ArrayList<>();

        userXCoords_temp = new ArrayList<>();
        userYCoords_temp = new ArrayList<>();


        userXCoords = new ArrayList<>();
        userYCoords = new ArrayList<>();



        constraintLayout = (ConstraintLayout) findViewById(R.id.scrollViewNestedConstraintLayout);

        //arraylait to store the number of coordinates that the user has to enter from 3 to n
        ArrayList<Integer> numberOfPointsArrayList = new ArrayList<>();
        int  numOfPoints = 100;
        //populating the arraylist with the numbers from 3 to n
        for (int k = 3; k <= numOfPoints; k++){
            numberOfPointsArrayList.add(k);
        }

        final Spinner numberOfPointsSpinnner = (Spinner) findViewById(R.id.numberOfPointsSpinner);
        ArrayAdapter<Integer> numberOfPointsAdapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,numberOfPointsArrayList);
        numberOfPointsSpinnner.setAdapter(numberOfPointsAdapter);



        numberOfPointsSpinnner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                removeCoordinateEditTexts();

                generateCoordinateEditTexts((Integer) adapterView.getSelectedItem());


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(xEditTextsId.size() > 0) {

            EditText lastXCoordEditText = (EditText) findViewById(xEditTextsId.get(0));
            lastXCoordEditText.setOnKeyListener(new EditText.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        submitData(numberOfPointsSpinnner);
                        Log.i("Here","Good");
                        return true;
                    }

                    return false;
                }
            });

            EditText lastYCoordEditText = (EditText) findViewById(yEditTextsId.get(0));
            lastYCoordEditText.setOnKeyListener(new EditText.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        submitData(numberOfPointsSpinnner);
                        return true;
                    }

                    return false;
                }
            });

        }







    }
}
